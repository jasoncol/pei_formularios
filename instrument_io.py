# -*- coding: latin-1 -*-
"""M�dulo con definiciones para cargar y guardar instrumentos en archivos
de disco."""
import json
import os
from popups import Popups
from logger import logger

EXTENSION = ".pei"


class InstrumentIO:

    """ Input/Output  de archivos de instrumento. Para cargar/guardar
    instrumentos. """

    @classmethod
    def load_instrument(cls, filename):
        """ Carga data de instrumento desde filename y devuelve instrumento
        instanciado deacuerdo al tipo.
        :param filename: nombre de archivo de instrumento.
        :returns: Instancia de instrumento cargado.
        """

        from instrument.instrumento_impacto import InstrumentoImpacto
        from instrument.instrument2 import Instrument2
        from instrument.base import INSTRUMENT_TYPE_1, INSTRUMENT_TYPE_2

        filename = filename + EXTENSION if '.pei' not in filename else filename
        fname = "./data/%s" % (filename)
        print "Loading from ", fname

        try:
            with open(fname, "r") as f:
                st = f.read()
                data = json.loads(st)
                header = data['header']

                type_ = data['header']['type']

                if type_ == INSTRUMENT_TYPE_1:
                    loaded_instrument = InstrumentoImpacto()
                elif type_ == INSTRUMENT_TYPE_2:
                    loaded_instrument = Instrument2()

                loaded_instrument.instrument_type = type_
                loaded_instrument.instrument_version = header['version']
                loaded_instrument.instrument_versiondate = header[
                    'version_date']
                loaded_instrument.date = header['date']
                loaded_instrument.name = header['name']
                loaded_instrument.construction_start_date = header['construction_start_date']
                loaded_instrument.construction_finish_date = header['construction_finish_date']
                loaded_instrument.intervention_type = header['intervention_type']
                loaded_instrument.observations = header['observations']
                loaded_instrument.data = data['data']
                loaded_instrument.fname = filename
                if 'submitter_name' in header:  # TODO temporal
                    loaded_instrument.submitter_name = header['submitter_name']
                return loaded_instrument
        except Exception as e:
            print "Couldn't load instrument IO error"
            logger.error("No se pudo cargar instrumento %s." %
                         (fname), exc_info=True)
            raise InstrumentIOException()

    @classmethod
    def save_instrument(cls, instrument, filename):
        """ Guarda instrument en filename.pei
        :param instrument: Instancia de instrumento a guardar.
        :param filename: Nombre de archivo de disco donde se va  a guardar
        el instrumento.
        """

        filename = filename + EXTENSION if '.pei' not in filename else filename
        fname = "./data/%s" % (filename)
        print "Saving data to ", fname
        data_header = {"name": instrument.name, "date": instrument.date,
                       "version": instrument.instrument_version,
                       "version_date": instrument.instrument_versiondate,
                       "type": instrument.instrument_type,
                       "submitter_name": instrument.submitter_name,
                       "construction_start_date": instrument.construction_start_date,
                       "construction_finish_date": instrument.construction_finish_date,
                       "intervention_type": instrument.intervention_type,
                       "observations": instrument.observations}
        data_questions = instrument.data

        data = {"header": data_header, "data": data_questions}
        json_out = json.dumps(data)

        try:
            with open(fname, "w") as f:
                f.write(json_out)
        except Exception as e:
            logger.error("No se pudo guardar instrumento %s" %
                         (fname), exc_info=True)
            raise Exception

    @classmethod
    def remove_instrument(cls, filename):
        """  Elemina el instrumento guardado en filename.pei
        :param filename: Nombre del archivo de disco que se va a eliminar
        """

        filename = filename + EXTENSION if '.pei' not in filename else filename
        fname = "./data/%s" % (filename)
        print "Deleting file ", fname

        try:
            os.remove(fname)
            Popups.info_popup(
                title="Instrumento eliminado",
                msg='El instrumento ha sido eliminado exitosamente')

        except OSError:
            Popups.info_popup(title='Instrumento no eliminado',
                              msg='El instrumento no se ha podido eliminar.')


class InstrumentIOException(Exception):

    """ Excepcion en operaciones IO de instrumento"""
    pass
