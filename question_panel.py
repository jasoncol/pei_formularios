# -*- coding: utf-8 -*-
""" Módulo contiene la clase QuestionPanel usada para presentar las preguntas
en el tab de diligenciamiento"""
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.stacklayout import StackLayout
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty,
                             BoundedNumericProperty)
from kivy.factory import Factory
from kivy.uix.label import Label
from question import ListQuestion
from base import KvPropertiesMixin
from kivy.clock import Clock

class QuestionPanel(KvPropertiesMixin, Screen):

    ''' A QuestionPanel is a panel that displays one or more questions '''
    questions = ListProperty([])
    questions_widgets = ListProperty([])
    qheights = ListProperty([])
    """ Puede ser un listado de size_hints_y (0 a 1) correspondiente a cada pregunta del qpanel.
    Tambien pueden ser tamaños fijos (listado de heights), en ese caso el height del qpanel se 
    calcula como la suma de estos valores"""
    layout = ObjectProperty(None)  # Internal layout
    title = StringProperty('')
    panel_height = NumericProperty(1000)
    """Height of the panel (affects scrollbar)"""
    instrument = ObjectProperty(None)
    """Instrumento actual"""
    last = BooleanProperty(False)
    auto_number = True
    """Si es True autonumerará las preguntas que contiene segun su posicion en el instrumento asociado"""
    ipm = None

    scrollview = ObjectProperty(None)

    def on_questions(self, obj, value):
        ''' Agrega preguntas a este panel usando un listado de nombres de preguntas (definidos en los kv)
        En caso de un refresco se verifica que el listado actual de widgets concuerde con el nuevo listado.
        '''
        print "setting questions", value
        # Remover preguntas que ya no estan en este panel segun el nuevo listado de preguntas
        to_rem = []
        for wid in self.questions_widgets:
            if wid.__class__.__name__ not in value:
                to_rem.append(wid)

        for wid in to_rem:
            self.layout.remove_widget(wid)
            del self.questions_widgets[self.questions_widgets.index(wid)]

        # Setear questions
        for i, question_name in enumerate(value):
            # Si la pregunta ya estaba agregada continuar con la siguiente
            if any([x.__class__.__name__ == question_name for x in self.questions_widgets]):
                continue

            #qh = self.qheights[i] if i < len(self.qheights) else 1.0
            qwidget = getattr(Factory, '%s' %
                              (question_name))()

            qwidget.qpanel = self
            self.questions_widgets.append(qwidget)
            qwidget.bind(height=self.increase_size) # Como el height es fijo se bindea a increase_size
                                                    # para setear panel_height
            qwidget.size_hint_y = None

            self.layout.add_widget(qwidget, index=-(i+1))
            self.increase_size(self, None)

    def on_last(self, obj, value):
        if self.last:
            self.layout.add_widget(Label(size_hint=(1,None), height=40,
                text="Final del diligenciamiento. Para validarlo y enviarlo por favor diríjase a la pestaña Enviar Registro", font_size=22, color=(1, 0, 0, 1), valign= 'bottom'))
            self.increase_size(self, None)
    

    def on_instrument(self, obj, value):

        print "seteando numeraciones"
        for qwidget in self.get_questions():
            qwidget.instrument = self.instrument
            if self.auto_number:
                pos = self.instrument.questions.index(qwidget.__class__.__name__)
                qwidget.number = pos + 1

    def get_questions(self):
        return self.questions_widgets

    def get_clsname(self):
        return self.__class__.__name__


    def increase_size(self, obj, value):
        """ Este método es util para mantener sincronizado la altura del qpanel (panel_height)
        con la suma de las alturas de los widgets cuando estos usan alturas fijas y no proporcionales
        """
        h = 0
        for q in self.questions_widgets:
            h += q.height + 5 # 5 es el spacing del box layout
        self.panel_height = h + 30 # 30 es la altura del label inicial (titulo dfel qpanel)

    @classmethod
    def get_kv_props(cls):
        properties, rule = super(QuestionPanel, cls).get_kv_props()
        if 'questions' in properties:
            properties['questions'] = eval(properties['questions'].value)

        return properties


    def on_touch_down(self, p):
        super(QuestionPanel, self).on_touch_down(p)
        if self.collide_point(p.x, p.y) and p.button == 'left':
            Clock.schedule_once(self.refresh_panel, 0.2) #TODO peligroso

    def refresh_panel(self, t):
        print "refreshing panel", t
        self.ipm.refresh_panel()

    def scroll_to(self, pos_y):
        """Scrollea hasta la posicion especificada solo si esta no se está mostrando actualmente (segun upper y lower limit).
        :param pos_y: pos_y absoluta.
        """
        #Normalizar pos_y
        real_posy = pos_y / self.layout.height
        scroll_height = self.scrollview.height #la altura del scrollview
        cur_posy = self.scrollview.scroll_y

        upper_limit = scroll_height + (self.layout.height -  scroll_height) * cur_posy
        lower_limit = upper_limit - scroll_height
        if not(lower_limit <= pos_y + 50 <= upper_limit):
            self.scrollview.scroll_y = real_posy
