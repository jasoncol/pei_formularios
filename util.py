# -*- coding: utf-8 -*-
# Este módulo contiene funciones de utilidad generales.
from kivy.core.image import Image
import constants as ct

def img2text(fname, scale, wrap='repeat'):
    """ Devuelve la textura de una imagen.

    :param fname: ruta de la imagen
    :param scale: Tamaño de la textura
    :param wrap: Texture.wrap
    :returns: Texture
    """
    texture = Image(fname).texture
    texture.wrap = wrap
    texture.uvsize = (scale, -scale)

    return texture

def get_departamentos_list():
    """ Devuelve lista de departamentos """
    return [x[1] for x in ct.DEPARTAMENTOS]
