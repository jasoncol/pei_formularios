# -*- coding: latin-1 -*-
""" Este m�dulo contiene las deficiones del panel de diligenciamiento del
instrumento donde se muestran las preguntas de este."""

from kivy.uix.screenmanager import ScreenManager
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty,
                             BoundedNumericProperty)
from kivy.factory import Factory
from instrument.instrumento_impacto import InstrumentoImpacto
from instrument_io import InstrumentIO
from kivy.uix.screenmanager import Screen
from popups import Popups
from question_panel import QuestionPanel
import time
from collections import OrderedDict

class InstrumentPanelManager(ScreenManager):

    """Panel Manager para diligenciamiento del instrumento.
    Muestra las preguntas del instrumento actual.

    Contiene controles de navegaci�n (bot�n anterior, siguiente y listado
    de subsecciones) para navegar entre las preguntas.

    T�cnicamente instancia y muestra los QuestionPanel del instrumento
    actual (cargado en self.load_instrument) seg�n se define en el atributo
    qpanels de este (instrument.qpanels) respetando el �rden de los mismos.

    Los eventos importantes son:
    - prev_panel: Llamado cuando se presiona el bot�n Anterior. Actualiza el id
    del qpanel actual (curpanel_id).
    - next_panel: Llamado cuando se presiona el bot�n Siguiente. Actualiza el
    id del qpanel actual.
    - on_curpanel_id: Llamado cuando se actualiza curpanel_id. Carga y muestra
    el QuestionPanel que tiene ese curpanel_id.
    """
    num_panels = NumericProperty(0)
    """N�mero de QuestionPanel del instrumento actual."""

    curpanel_id = NumericProperty(-1)
    """Identificador del QuestionPanel actual. Es la posicion del QPanel en el
    listado instrument.qpanels"""

    loaded_panels = []  # ListProperty([])
    """Listado de las instancias de los QuestionPanel cargados hasta el
    momento"""

    instrument = ObjectProperty(None)
    """Instrumento Actual"""

    last_action = StringProperty('prev')
    """String que dice cual fue la �ltima acci�n del usuario en cuanto a
    navegaci�n (si hizo prev, next, etc.)"""

    current_label = ObjectProperty(None)
    """Label que muestra el nombre del instrumento actual"""

    sections_ctrl = ObjectProperty(None)
    """Referencia al Spinner que muestra el listado de las subsecciones del
    instrumento."""

    curpanel = None
    panel_title = ""
    questions_per_panel = 3
    questions_sections = OrderedDict()

    def __init__(self, *args, **kwargs):
        super(InstrumentPanelManager, self).__init__(*args, **kwargs)

        # self.instrument = InstrumentIO.load_instrument("test")
        # self.reset()

    def on_sections_ctrl(self, obj, value):
        """ Hace binding del evento de cambio de valor de la subsecci�n actual
        en sections_control a self.section_selected. """
        self.sections_ctrl.bind(text=self.section_selected)

    def reset(self):
        """ Resetea el panel. """
        self.current = "default_screen"
        self.loaded_panels[:] = []
        # self.clear_widgets()
        to_del = [x for x in self.screens if x.name != "default_screen"]
        for i in to_del:
            self.remove_widget(i)
            self.real_remove_widget(i)
        self.curpanel = None

    def load_instrument(self, instrument):
        """Carga el instrumento en este panel y lo referencia
        :param instrument: instancia de Instrumento.
        """
        from kivy.app import App
        App.get_running_app().load_instrument_kv(instrument.__class__)
        self.reset()
        self.instrument = instrument
        for question in self.instrument.questions:
            #section = ""
            for q, s in self.instrument.sections_inv.iteritems():
                if q == question:
                    section = self.instrument.sections_inv[q]
            #self.questions_sections.append((question, section))
            self.questions_sections[question] = section
        print self.questions_sections
        self.instrument.bind(name=self.set_name_lbl)
        #self.current_label.text = "Instrumento: %s" % (self.instrument.name)
        self.num_panels = len(self.instrument.qpanels)
        self.sections_ctrl.values = self.instrument.sections.keys()
        #self.curpanel_id = 0
        # Obliga a generar el evento
        #self.property('curpanel_id').dispatch(self)
        # on_curpanel_id
        self.show_questions()

    def set_name_lbl(self, obj, value):
        #self.current_label.text = "Instrumento: %s" % (self.instrument.name)
        pass

    def refresh_panel(self):
        self.show_questions('refresh')

    def show_questions(self, action=None, question_pos=None):
        """Crea el QPanel y le carga las preguntas que correspondientes segun action y
         el panel anterior y sus preguntas. En caso de refresh se refresca el listado
         de preguntas del Qpanel actual.
         :param action: puede ser None, 'next', 'prev', 'refresh', 'goto'
         """

        cp = self.curpanel
        if cp is None: # No hay panel actual, mostrar primeras preguntas
            start = 0
            finish = len(self.instrument.questions)
            increment = 1
        elif action == 'next':
            start = self.instrument.questions.index(cp.questions[-1]) + 1 #empezar despues de la ultima pregunta
            finish = len(self.instrument.questions)
            increment = 1
        elif action=='prev':
            # Toca agregar preguntas de para atras
            start = self.instrument.questions.index(cp.questions[0]) - 1 
            finish = -1 #el -1 es para que incluya el cero
            increment = -1
        elif action == 'refresh':
            self.update_instrument()
            start = self.instrument.questions.index(cp.questions[0])
            finish = len(self.instrument.questions)
            increment = 1
        elif action == 'goto':
            start = question_pos
            finish = len(self.instrument.questions)
            increment = 1

        questions2add = self._get_question_list(start, finish, increment)

        self.panel_title = self._get_panel_title(questions2add, self.panel_title)

        if len(questions2add) == 0: # No hay preguntas para mostrar
            return

        if action == 'refresh' and self.curpanel is not None:
            qpanel = self.curpanel
            qpanel.questions = questions2add
            qpanel.instrument = self.instrument
            qpanel.on_instrument(self, self.instrument) # Forzar evento
        else:
            qpanel = QuestionPanel() # TODO cacheo?
            qpanel.name = 'QPanel_%s'%(time.time())
            qpanel.title = self.panel_title
            self.add_widget(qpanel)
            qpanel.ipm = self
            qpanel.questions = questions2add
            qpanel.instrument = self.instrument

        # Cargar respuestas del instrumento
        for question in qpanel.get_questions():
                question.load_data(self.instrument.
                                   get_question_data(question.qid))

        oldpanel = self.curpanel
        self.curpanel = qpanel
        self.current = qpanel.name
        if self.instrument.questions[-1] in self.curpanel.questions:
            self.curpanel.last = True
        print "Mostrar panel con preguntas", questions2add
        # Quitar panel viejo
        if oldpanel is not None and action != 'refresh':
            to_del = [x for x in self.screens if x.name != "default_screen" and x.name != self.current]
            for i in to_del:
                self.remove_widget(i)
                self.real_remove_widget(i)

    def _get_question_list(self, start, finish, increment):
        """Devuelve el listado de preguntas a agregar"""
        questions2add = []
        for i in range(start, finish, increment):
                # TODO dependencia
                if self.instrument.execute_rules(self.instrument.questions[i]):
                    stop = False
                    if len(questions2add) != 0 or increment == -1:
                        for t, q in self.instrument.sections.iteritems():
                            if q == self.instrument.questions[i]:
                                stop = True
                    if stop:
                        # Si es siguiente (increment=1) hacer el corte y no incluir esta pregunta cabeza de seccion
                        if increment != -1: 
                            break
                        else: # Si es anterior (increment=-1) incluir esta pregunta cabeza de seccion y hacer el corte
                            questions2add.append(self.instrument.questions[i])
                            break
                    else:
                        questions2add.append(self.instrument.questions[i])

                if len(questions2add) == self.questions_per_panel:
                    break
        if increment == -1:
            questions2add.reverse() # obtener el orden correcto si se agregaron de para atras
        return questions2add

    def _get_panel_title(self, questions, curtitle):
        """Devuelve el titulo de la seccion actual"""
        title = curtitle
        for i in questions:
            if self.questions_sections[i] != curtitle:
                title = self.questions_sections[i]
            break
        return title

    def prev_panel(self, update=True):
        """Llamado cuando se presiona el bot�n Anterior. Muestra las
        preguntas anteriores a las actuales.

        :param update: Booleano que si es True llama a self.update_instrument
        para actualizar la informaci�n del instrumento.
        """

        print "prev"
        if update:
            self.update_instrument()
        self.last_action = 'prev'
        self.show_questions('prev')

    def next_panel(self, update=True):
        """Llamado cuando se presiona el bot�n Siguiente. Muestra las
        preguntas anteriores a las actuales.

        :param update: Booleano que si es True llama a self.update_instrument
        para actualizar la informaci�n del instrumento.
        """

        print "next"
        if update:
            self.update_instrument()
        self.last_action = 'next'
        self.show_questions('next')

    def update_section_ctrl(self):
        """ Actualiza el valor section_ctrl seg�n el QPanel que se est�
        mostrando actualmente. """
        for k, v in self.instrument.sections.iteritems():
            if self.instrument.questions[self.curpanel_id] == v and \
               self.sections_ctrl.text != k:
                self.sections_ctrl.text = k
                break

    def goto(self, question_pos):
        """Muestra las pregunta partiendo de la pregunta en esa posicion.

        :param question_pos: Posici�n de la pregunta en el listado."""
        self.update_instrument()
        self.show_questions('goto', question_pos)
        
    def section_selected(self, obj, value):
        """ Muestra las preguntas de la subsecci�n seleccionada en sections_ctrl.
        Se llama cuando el usuario selecciona una secci�n."""
        try:
            #panel_num = self.instrument.qpanels.index(self.instrument.
            #                                          sections[value])
            question_num = self.instrument.questions.index(self.instrument.sections[value])
            self.goto(question_num)
        except:
            pass

    def update_instrument(self):
        """ Actualiza la data del instrumento actual asign�ndole la  informacion
        de las preguntas actuales del QuestionPanel que se est� mostrando.
        Esto actualiza instrument.data"""

        current_qp = self.curpanel
        for q in current_qp.get_questions():
            self.instrument.set_question_data(q)

    def save(self):
        """ Guarda instrumento actual """
        self.update_instrument()  # Obtener data de preguntas actuales primero
        try:
            self.instrument.save_data()
            Popups.info_popup(
                title="Instrumento guardado",
                msg='El instrumento ha sido guardado exitosamente')

        except:
            Popups.info_popup(
                title='Instrumento no guardado',
                msg='El instrumento no se ha podido guardar')
