# -*- coding: utf-8 -*-
from base import SubQuestion, EmptyValueValidator
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy.factory import Factory

class DatePickerField(BoxLayout, SubQuestion):

    """ Subpregunta para seleccionar  fecha.

    data = ['year', 'month', 'day'] month segun self.MONTH_LIST
    """
    default_validators = [EmptyValueValidator()]
    FIRST_YEAR = 1980
    """ Primer año """

    MONTH_LIST = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                  "Diciembre"]

    DAY_LIST = [str(i + 1) for i in range(31)]

    def __init__(self, *args, **kwargs):
        super(DatePickerField, self).__init__(*args, **kwargs)
        self.create_datelists()  # Crear listados

    def create_datelists(self):
        """Crea los listados para año, mes y dia """
        import datetime
        current_year = datetime.datetime.now().year
        year_list = [str(x) for x in range(current_year,
                                           DatePickerField.FIRST_YEAR - 1, -1)]
        self.year_list_widget = Factory.DatePickerPopupContent()
        for year in year_list:
            tb = Factory.ToggleButtonS(text=year, group="year_%s" % (self))
            tb.bind(on_press=self.close_popup)
            self.year_list_widget.sv.add_widget(tb)

        self.year_list_widget.sv.height = 40 * len(year_list) + 10

        self.month_list_widget = Factory.DatePickerPopupContent()
        for month in DatePickerField.MONTH_LIST:
            tb = Factory.ToggleButtonS(text=month, group="month_%s" % (self))
            tb.bind(on_press=self.close_popup)
            self.month_list_widget.sv.add_widget(tb)

        self.month_list_widget.sv.height = 40 * len(DatePickerField.MONTH_LIST)\
            + 10

        self.day_list_widget = Factory.DatePickerPopupContent()
        for day in DatePickerField.DAY_LIST:
            tb = Factory.ToggleButtonS(text=day, group="day_%s" % (self))
            tb.bind(on_press=self.close_popup)
            self.day_list_widget.sv.add_widget(tb)

        self.day_list_widget.sv.height = 40 * len(DatePickerField.DAY_LIST)\
            + 10

    def get_data(self):
        data = []
        for container in (self.year_list_widget.sv, self.month_list_widget.sv,
                          self.day_list_widget.sv):
            # Obtiene el togglebutton que esta down en cada listado
            value = [x.text for x in container.children if x.state == 'down']
            value = value[0] if len(value) else None
            data.append(value)

        return data

    def load_data(self, data):
        """data = ['year', 'month', 'day'] month segun self.MONTH_LIST"""
        if data is None or not isinstance(data, list):
            self._update_btns_values()
            return
        year, month, day = data
        for tb in self.year_list_widget.sv.children:
            if tb.text == year:
                tb.state = 'down'

        for tb in self.month_list_widget.sv.children:
            if tb.text == month:
                tb.state = 'down'

        for tb in self.day_list_widget.sv.children:
            if tb.text == day:
                tb.state = 'down'

        self._update_btns_values()

    def show_popup(self, mode):

        if mode == 'year':
            self.year_list_widget.parent = None
            self._popup = Popup(title="Seleccione Año",
                                content=self.year_list_widget)
        elif mode == 'month':
            self.month_list_widget.parent = None
            self._popup = Popup(title="Seleccione Mes",
                                content=self.month_list_widget)
        else:
            self.day_list_widget.parent = None
            self._popup = Popup(title="Seleccione Día",
                                content=self.day_list_widget)

        self._popup.open()

    def close_popup(self, val):
        self._popup.dismiss()
        self._update_btns_values()

    def _update_btns_values(self):
        data = self.get_data()
        year, month, day = data
        """Actualiza el valor de los botones que ve el usuario"""
        self.children[2].text = 'Año: %s' % (year) if year is not None else\
            'Año: -'
        self.children[1].text = 'Mes: %s' % (month) if month is not None else\
            'Mes: -'
        self.children[0].text = 'Día: %s' % (day) if day is not None else\
            'Día: -'

    def _paint_error(self, error):
        pass

    @classmethod
    def get_data_str(cls, **kwargs):
        data = kwargs['data']
        if data is None or None in data:
            return ''
        return "-".join(data)
