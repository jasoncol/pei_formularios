# -*- coding: utf-8 -*-
from base import SubQuestion, EmptyValueValidator
from kivy.factory import Factory
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.popup import Popup
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty)


class ListField(Factory.SpinnerS, SubQuestion):

    ''' Listfield . Define entries with values
        ej: values = (1, 2, 3, 4)
    '''
    default_validators = [EmptyValueValidator()]
    values_ids = ListProperty([])  # Los id internos de las opciones (values)

    default_text = StringProperty(" - ")
    selected = False

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)
        self.bind(text=self.value_selected)
        self.option_cls = Factory.SpinnerOptionS

    def on_values(self, obj, values):
        if len(self.values_ids) == 0:
            self.values_ids.extend([x + 1 for x in range(len(values))])

    def value_selected(self, obj, value):
        if value != self.default_text:
            self.selected = True
        else:
            self.selected = False

    def get_data(self):
        try:
            ind = self.values_ids[self.values.index(self.text)]
        except:
            ind = None

        return ind

    def set_props(self, **kwargs):
        if 'group' in kwargs:
            self.group = kwargs['group']
        if 'values' in kwargs:
            self.values = kwargs['values']
            self.property('values').dispatch(self)

    def load_data(self, data):
        if data is not None and int(data) > 0:
            ind = int(data)
            if ind in self.values_ids:
                self.text = self.values[self.values_ids.index(ind)]
        else:
            self.text = self.default_text

    def _paint_error(self, error):
        pass

    @classmethod
    def get_data_str(cls, **kwargs):
        if not 'values_ids' in kwargs:
            values_ids = [i+1 for i, x in enumerate(kwargs['values'])]
        else:
            values_ids = kwargs['values_ids']

        data = kwargs['data']
        if data is None or data == 0:
            return ''
        values = kwargs['values']

        data_str = values[values_ids.index(data)]
        return '|'.join(data_str)

class OptionsField(BoxLayout, SubQuestion):
    default_validators = [EmptyValueValidator()]

    """ Used by ToggleField and CheckBoxGroupField. Do not use directly """

    use_popup = BooleanProperty(False)
    """ Si use_popup es True el listado se muestra en un popup que se accede
    mediante un boton """
    values = ListProperty([])
    values_ids = ListProperty([])  # Los id internos de las opciones (values)
    group = ObjectProperty(None, allowednone=True)
    option_class = ObjectProperty(None)
    container = ObjectProperty(None)
    """ El contenedor del listado. Si use_popup es False se usa el mismo objeto
    (self) como contenedor.
    Sino se usa otro BoxLayout que se muestra en un popup """
    btn_popup = ObjectProperty(None)
    btn_popup_maxlen = NumericProperty(200)
    """Boton que abre el popup"""
    container_height = NumericProperty(1500)
    """Altura de contenedor"""

    def __init__(self, *args, **kwargs):
        super(OptionsField, self).__init__(*args, **kwargs)
        self.bind(values=self.set_values)
        self.container = self
        self._popup = None

    def set_values(self, obj, values):

        # Definir que container utilizar para los hijos
        if not self.use_popup:
            self.container = self
        else:
            # Configurar  como popup
            if self.btn_popup is None:
                self.btn_popup = Factory.ButtonS(text="-")
                self.add_widget(self.btn_popup)
                self.container = BoxLayout(
                    size_hint_y=None, height=self.container_height)
                self.btn_popup.bind(on_press=self.show_popup)

        self.container.clear_widgets()
        for v in values:
            tb = self.option_class(text=v, group=self.group)
            tb.bind(on_press=self.close_popup)
            self.container.add_widget(tb)
        if len(self.values_ids) == 0:
            self.values_ids.extend([x + 1 for x in range(len(values))])

    def on_container_height(self, obj, value):
        if self.use_popup:
            self.container.height = self.container_height

    def show_popup(self, o):
        """Muestra el popup. Bind de btn_popup.on_press"""
        self.container.parent = None
        # TODO pasar toda esta estructura de widget a kv
        bl_content = BoxLayout(orientation='vertical', spacing=5)
        sv = Factory.ScrollViewS(size_hint=(1, 1))
        sv.add_widget(self.container)
        bl_content.add_widget(sv)
        self._popup = Popup(title="-", content=bl_content)

        self._popup.open()

    def close_popup(self, o):
        """Cierra el popup y actualiza el valor de btn_popup"""
        if self._popup is None:
            return
        self._popup.dismiss()

        data = self.get_data()
        if len(data):
            data_text = [self.values[self.values_ids.index(x)] for x in data]
            btn_text = ', '.join(data_text)
            if len(btn_text) > self.btn_popup_maxlen:
                btn_text = "%s..."%(btn_text[:self.btn_popup_maxlen])
            self.btn_popup.text = btn_text

        else:
            self.btn_popup.text = '-'

        self.mark_error()
        self._popup = None

    def set_props(self, **kwargs):
        if 'group' in kwargs:
            self.group = kwargs['group']
        if 'values' in kwargs:
            self.values = kwargs['values']
            self.property('values').dispatch(self)
        if 'orientation' in kwargs:
            self.container.orientation = kwargs['orientation']

    def _paint_error(self, error):
        pass


class ToggleField(OptionsField):

    """ Similar to listfield (list of options) but using togglebuttons instead .
    Use orientation 'horizontal' and 'vertical' to change orientation.
    Use group = "[unique_string]" to allow multiple selection.
    """

    def __init__(self, *args, **kwargs):
        super(ToggleField, self).__init__(*args, **kwargs)
        self.option_class = Factory.ToggleButtonS

    def get_data(self):
        val = [self.values_ids[i] for i, x in enumerate(
            self.container.children[::-1]) if x.state == 'down']
        if len(val):
            return val
        return []

    def load_data(self, data):

        if self.btn_popup is not None:
            self.btn_popup.text = '-'

        if data is None:
            return
        btn_popup_text = []
        for i, c in enumerate(self.container.children[::-1]):
            id_ = self.values_ids[i]
            if id_ in data:
                c.state = 'down'
                if self.use_popup:
                    btn_popup_text.append(self.values[i])
            else:
                c.state = 'normal'

        if len(btn_popup_text) and self.use_popup:
            btn_text = ', '.join(btn_popup_text)
            if len(btn_text) > self.btn_popup_maxlen:
                btn_text = "%s..."%(btn_text[:self.btn_popup_maxlen])

            self.btn_popup.text = btn_text
    
    
    @classmethod
    def get_data_str(cls, **kwargs):
        data = kwargs['data']
        if data is None:
            return ''

        if not 'values_ids' in kwargs:
            values_ids = [i+1 for i, x in enumerate(kwargs['values'])]
        else:
            values_ids = kwargs['values_ids']

        values = kwargs['values']

        data_str = [values[values_ids.index(x)] for x in data]
        return '|'.join(data_str)


class ClickLabel(ButtonBehavior, Label):

    """ Used by checkboxbutton """
    pass


class CheckBoxButton(BoxLayout):
    text = StringProperty('')
    group = ObjectProperty(None, allowednone=True)
    checkbox = ObjectProperty(None)
    label = ObjectProperty(None)
    textfield = ObjectProperty(None)
    show_textfield = BooleanProperty(True)

    def on_text(self, obj, value):
        if self.label is not None:
            self.label.text = value

    def on_group(self, obj, value):
        if self.checkbox is not None:
            self.checkbox.group = value

    def on_checkbox(self, obj, value):
        self.checkbox.group = self.group

    def on_label(self, obj, value):
        self.label.text = self.text

    def on_show_textfield(self, obj, value):
        if value is False and self.textfield in self.children:
            self.remove_widget(self.textfield)

from base import EmptyFieldException


class CheckGroupFieldValidator(EmptyValueValidator):

    def validate(self, sq_tag, value):
        super(CheckGroupFieldValidator, self).validate(sq_tag, value)
        if not len(value[0]):
            raise EmptyFieldException(sq_tag)

class CheckBoxGroupField(OptionsField):

    """ Similar to listfield (list of options) but using checkboxbutton
    (or radio) instead.
    Use orientation 'horizontal' and 'vertical' to change orientation.
    Use group = "[unique_string]" to allow multiple selection (checkbox).
    if None radiobuttons are used.
    Use show_textfields = True to show textfields for each option.
    """
    default_validators = [CheckGroupFieldValidator()]
    show_textfields = BooleanProperty(True)

    def __init__(self, *args, **kwargs):
        super(CheckBoxGroupField, self).__init__(*args, **kwargs)
        self.option_class = CheckBoxButton

    def on_show_textfields(self, obj, value):
        for i in self.children:
            i.show_textfield = value

    def get_data(self):
        options = [self.values_ids[i]
                   for i, x in enumerate(self.children[::-1])
                   if x.checkbox.active]

        texts = [x.textfield.textinput.text
                 for x in self.children[::-1]
                 if x.checkbox.active and x.show_textfield]

        return options, texts

    @classmethod
    def cls_validate(cls, sq_props):
        try:
            super(CheckBoxGroupField, cls).cls_validate(sq_props)
        except EmptyFieldException as e:
            if 'group' in sq_props and sq_props['group'] is not None:
                raise e

    def load_data(self, data):
        if data is not None:
            options, texts = data
            for i, x in enumerate(self.children[::-1]):
                id_ = self.values_ids[i]
                if id_ in options:
                    x.checkbox.active = True

                    if x.textfield is not None and \
                       options.index(id_) < len(texts):
                        x.textfield.textinput.text = texts[options.index(id_)]
                else:
                    x.checkbox.active = False
    @classmethod
    def get_data_str(cls, **kwargs):
        data = kwargs['data']
        if data is None:
            return ''
        values = kwargs['values']

        if not 'values_ids' in kwargs:
            values_ids = [i+1 for i, x in enumerate(kwargs['values'])]
        else:
            values_ids = kwargs['values_ids']


        options, texts = data
        d = []
        for i, x in enumerate(options):
            d.append(values[values_ids.index(x)])
            if i < len(texts):
                d.append("Comentario: ")
                d.append(texts[i])

        return "\n ".join(d)
