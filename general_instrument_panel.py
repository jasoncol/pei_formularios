# -*- coding: utf-8 -*-
""" Este módulo contiene las deficiones del panel general del instrumento donde
se muestra un formulario con los datos básicos del mismo."""

from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, ListProperty
from popups import Popups


class GeneralInstrumentPanel(BoxLayout):

    """Panel general del instrumento."""

    general_question = ObjectProperty(None)
    """Referencia al formulario (un MultipleQuestion con las subpreguntas de los
    datos básicos del instrumento)"""

    instrument = ObjectProperty(None)
    """Referencia al instrumento cargado"""

    instrument_mgr_tab = ObjectProperty(None)
    tabpanel = ObjectProperty(None)

    def on_tabpanel(self, obj, value):
        self.tabpanel.bind(current_tab=self._auto_save)

    def _auto_save(self, obj, value):
        """ callback de la propiedad current_tab del tabpanel de la UI. Si el
        tab actual es el de diligenciar se guarda automáticamente los datos
        generales """
        curtab = value
        if curtab == self.instrument_mgr_tab:
            self.save(False)

    def load_instrument(self, instrument):
        """Carga los datos del instrumento en el formulario y guarda una
        referencia del mismo (self.instrument)

        :param instrument: Instrumento instanciado.
        :returns: None.
        """

        type_str = {'1': "Instrumento Impacto",
                    '2': "Instrumento II: Indicadores de Calidad Educativa"}
        subqs = self.general_question.subquestions
        name_q = subqs['SQID-1']
        #type_q = subqs['SQID-2']
        #fname_q = name_q #subqs['SQID-2']
        date_q = subqs['SQID-2']
        name_enc_q = subqs['SQID-3']
        date_ini_q = subqs['SQID-4']
        date_fin_q = subqs['SQID-5']
        type_int_q = subqs['SQID-6']
        obs_q = subqs['SQID-7']

        name_q.load_data(instrument.name)
        #type_q.load_data(type_str[instrument.instrument_type])
        #fname_q.load_data(instrument.fname[:-4])
        date_q.load_data(list(instrument.date.split('/')))
        name_enc_q.load_data(instrument.submitter_name)
        date_ini_q.load_data(list(instrument.construction_start_date.split('/')))
        date_fin_q.load_data(list(instrument.construction_finish_date.split('/')))
        try:
            type_int_q.load_data(instrument.intervention_type)
        except:
            pass
        obs_q.load_data(instrument.observations)

        self.instrument = instrument

    def goto_instrument_manager(self):
        """ Muestra el panel de diligenciamiento del instrumento. Se llama al
        presionar el botón de diligenciamiento."""
        self.tabpanel.switch_to(self.instrument_mgr_tab)

    def save(self, save2file=True):
        """Guarda el instrumento actual.

        :param save2file: Si es True guarda el instrumento en disco.
        False actualiza los atributos del obj instrumento pero no guarda en
        disco.
        """

        if self.instrument is None:
            return
        subqs = self.general_question.subquestions
        name_val = subqs['SQID-1'].get_data()
        fname_val = name_val + '.pei' #subqs['SQID-2'].get_data() + '.pei'
        date_val = subqs['SQID-2'].get_data()
        name_enc_val = subqs['SQID-3'].get_data()
        date_ini_val = subqs['SQID-4'].get_data()
        date_fin_val = subqs['SQID-5'].get_data()
        type_int_val = subqs['SQID-6'].get_data()
        obs_val = subqs['SQID-7'].get_data()

        try:
            self.general_question.validate()
            # TODO reglas más avanzadas para validar
        except Exception as e:
            Popups.info_popup(
                msg='Error en los datos generales del instrumento. %s' % (e))
        else:
            self.instrument.fname = fname_val
            self.instrument.name = name_val
            self.instrument.date = "%s/%s/%s" % (date_val[0], date_val[1], date_val[2])
            self.instrument.submitter_name = name_enc_val
            self.instrument.construction_start_date = "%s/%s/%s" % (date_ini_val[0], date_ini_val[1], date_ini_val[2])
            self.instrument.construction_finish_date = "%s/%s/%s" % (date_fin_val[0], date_fin_val[1], date_fin_val[2])
            self.instrument.intervention_type = type_int_val
            self.instrument.observations = obs_val
            self.instrument.validated = False

            if save2file:
                try:
                    self.instrument.save_data()
                    Popups.info_popup(
                        title="Instrumento guardado",
                        msg='El instrumento ha sido guardado exitosamente')

                except:
                    Popups.info_popup(
                        title='Instrumento no guardado',
                        msg='El instrumento no se ha podido guardar')
