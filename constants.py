# -*- coding: utf-8 -*-
""" Aquí se definen constantes globales el sistema """
import csv
WIDTH, HEIGHT = 1366, 768
PREESCOLAR = 1
BASICA_PRIMARIA = 2
BASICA_SECUNDARIA = 3
MEDIA = 4
EDUCACION_TIPO = (1, 2, 3, 4, 5, 6)


DEPARTAMENTOS = [
    (91, 'Amazonas'),
    (5, 'Antioquia'), (81, 'Arauca'),
    (8, 'Atlántico'),
    (13, 'Bolívar'), (15, 'Boyacá'),
    (17	, 'Caldas'),
    (18	, 'Caquetá'),
    (85	, 'Casanare'),
    (19	, 'Cauca'),
    (20, 'Cesar'),
    (27, 'Chocó'),
    (25, 'Cundinamarca'),
    (23, 'Córdoba'),
    (94, 'Guainía'),
    (95, 'Guaviare'),
    (41, 'Huila'),
    (44, 'La Guajira'),
    (47, 'Magdalena'),
    (50, 'Meta'),
    (52, 'Nariño'),
    (54, 'Norte de Santander'),
    (86, 'Putumayo'),
    (63, 'Quindío'),
    (66, 'Risaralda'),
    (88, 'San Andrés'),
    (68, 'Santander'),
    (70, 'Sucre'),
    (73, 'Tolima'),
    (76, 'Valle del Cauca'),
    (97, 'Vaupés'),
    (99, 'Vichada'), ]

SUBREGIONES = []
with open("subregiones.csv", 'r') as f:
    r = csv.reader(f, delimiter=',')
    for i, row in enumerate(r):
        if i:
            SUBREGIONES.append((int(row[0]), row[1], int(row[2])))

MUNICIPIOS = []
with open("municipios.csv", 'r') as f:
    r = csv.reader(f, delimiter=',')
    for i, row in enumerate(r):
        if i:
            MUNICIPIOS.append((int(row[0]), row[1], int(row[2]), int(row[3])))

MUNICIPIOS = sorted(MUNICIPIOS, key= lambda x: x[1])
