# -*- coding: latin-1 -*-
""" Este módulo contiene las deficiones de los tipos de pregunta (Question)."""

# Importante: No eliminar imports no usados pues pueden ser usados en el eval de Question.get_kv_props
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty,
                             BoundedNumericProperty, DictProperty)
from subquestion import (SubQuestion, ToggleField, ListField, NumberField,
                         TextField, TextArea, CheckBoxGroupField,
                         DatePickerField, FileField)
from kivy.factory import Factory as fc
LabelS = fc.LabelS
from kivy.uix.gridlayout import GridLayout
from kivy.uix.layout import Layout
from collections import OrderedDict
from kivy.lang import Builder
from base import KvPropertiesMixin
from logger import logger


class Question(KvPropertiesMixin, BoxLayout):

    """Clase padre de la que heredan todos los tipos de pregunta. Define la
    funcionalidad básica de una pregunta.
    Los métodos de clase (@classmethod) permiten acceder a los valores de 
    los parámetros definidos en los archivos kv para una pregunta en
    particular.
    """

    number = NumericProperty(0)  # Question Number that the user can see
    """ Número de pregunta. Número que se muestra al comienzo del enunciado de
    la pregunta """

    qid = NumericProperty(0)  # Internal ID of the question
    """ Identificador interno de la pregunta. Es el id que se usa para almacenar
    la información en un archivo de disco """

    text = StringProperty("")
    """Enunciado de la pregunta. Texto."""

    subtext = StringProperty("")
    """Subtexto Adicional"""

    qpanel = ObjectProperty(None)
    """Referencia al QuestionPanel al que pertenece la pregunta."""

    subquestions = {}
    """Diccionario con las instancias de las subpreguntas de la pregunta. Usa
    como llaves los SQID de las subpreguntas
    (ej {subq1.sqid: subq1, subq2.sqid: subq2}"""

    sqid_acum = 0
    """Numero, acumulador para asignar SQID a subpreguntas que no tengan"""

    visible = BooleanProperty(True)
    """Determina si la pregunta es visible  o no"""

    instrument = ObjectProperty(None)
    """Instrumento actual"""

    cls_external_subqs_num =0

    def __init__(self, *args, **kwargs):
        self.subquestions = OrderedDict()
        super(Question, self).__init__(*args, **kwargs)

    def save(self):
        # TODO deprecada
        raise NotImplementedError

    def get_data(self):
        """ Devuelve un diccionario con los valores de las respuestas de
        cada subpregunta. """

        data = {}  # Subquestions data
        for k, subq in self.subquestions.iteritems():
            data[subq.sqid] = subq.get_data()
        # import json
        # json_out = json.dumps(data)
        # print "question", self.qid
        # print "number of subquestions", len(self.subquestions)
        # print "data is", data
        return data

    def load_data(self, data):
        """ Asigna los valores en data a las subpreguntas.

        :param data: diccionario con información que usa SQID como llaves para
        saber a que subpregunta le corresponde el valor.
        """

        if data is None:  # No hay data para esta pregunta
            return
        for k, v in data.iteritems():
            try:
                self.subquestions[k].load_data(v)
            except Exception as e:
                logger.error("No se pudo cargar la info de subpregunta %s a pregunta %s"%(k, self.qid), exc_info=True)


    def add_widget(self, widget, index=0):
        """ Se reescribe add_widget para capturar instancias de subpreguntas y
        agregarlas a self.subquestions"""
        super(Question, self).add_widget(widget, index)
        # TODO get all subquestions inside other child widgets
        # Override add_widget to reference the subquestion when added
        if isinstance(widget, SubQuestion):
            # print "Adding subwidget", widget
            # Set identifier (sqid)
            self.add_subq(widget)
        # Si es un layout ver si sus hijos son Subquestions  y agregarlos
        elif isinstance(widget, Layout):
            # Chequear cuando se agreguen hijos a este subwidget
            widget.bind(children=self._check4subqs)

    def _check4subqs(self, obj, val):
        """Chequea si hay subpreguntas en val."""
        # val es widget.children, como se van agregando hijos a la lista solo
        # nos interesa el ultimo agregado
        last_child = val[0]
        if isinstance(last_child, SubQuestion):
            self.add_subq(last_child)

    def add_subq(self, subq):
        """ Agrega la subpregunta a self.subquestions.

        :param subq: Instancia de SubQuestion.

        """

        if hasattr(subq,'focus'):
            subq.bind(focus=self.subq_focused)
        if subq.sqid is not None or subq.sqid != "":
            self.sqid_acum += 1
            subq.sqid = "SQID-" + str(self.sqid_acum)
        if subq.sqid not in self.subquestions:
            self.subquestions[subq.sqid] = subq
            subq.question = self

    def get_subq(self, sqid=0, tag=''):
        """ Devuelve subquestion """
        if tag == '':
            return self.subquestions["SQID-" + str(sqid)]
        else:
            for k, v in self.subquestions.iteritems():
                if v.tag == tag:
                    return v
        return None  # TODO raise exception

    def validate(self):
        """Valida la pregunta validando todas las subpreguntas"""
        for k, x in self.subquestions.iteritems():
            x.validate()
        return True

    def subq_focused(self, obj, value):
        """Si una subpregunta de esta pregunta fue enfocada hacer que el qpanel scrollee hasta la pregunta"""
        print "PREGUNTA", self, "FOCUSEADA"
        if value and self.qpanel is not None:
            pos_y = obj.pos[1]
            self.qpanel.scroll_to(pos_y)

    @classmethod
    def cls_validate(cls, subquestions):
        for sq in subquestions:
            sq_cls, sq_props = sq
            sq_cls.cls_validate(sq_props)

    def __str__(self):
        return str(self.number) + ". " + self.text
    
    @classmethod
    def get_kv_props(cls):
        """Devuelve los valores de las propiedades definidos en el kv de esta pregunta. Ademas devuelve el listado de subpreguntas definidas en el kv con sus propiedades."""

        properties, rule = super(Question, cls).get_kv_props()
        question_clsobj = eval(rule.name[:-1].split('@')[1]) # Tipo de clase question (ej: MultipleQuestion)
        subqs = []
        # Obtener subquestions hijos
        cls._get_subq_children(subqs, rule.children)
        properties['clsobj'] = question_clsobj
        
        if 'text' in properties:
            properties['text'] = properties['text'].value
        if 'qid' in properties:
            properties['qid'] = properties['qid'].value

        return properties, subqs

    @classmethod
    def cls_load_data(cls, instrument, subqs, qdata):
        """Setea la entrada 'data' a subqs.props (devuelto por get_kv_props) obtenido de qdata.
        Es la version del load_data para la clase"""
        
        for cnt, sq in  enumerate(subqs):
            sq = subqs[cnt]
            sq[1]['data'] = instrument.get_subq_data(qdata, cnt+1)

    @classmethod
    def _get_subq_children(cls, subqs, children):
        for i in children:
            clsobj = eval(i.name) # TODO seguridad
            if not issubclass(clsobj, SubQuestion): #Revisar dentro de otros widgets
                cls._get_subq_children(subqs, i.children)
                continue

            props = {}
            for k, v in i.properties.iteritems():
                try:
                    props[k] = eval(v.value)
                except:
                    props[k] = v.value

            subqs.append((clsobj, props))


class SimpleQuestion(Question):

    """ Pregunta Simple para tipo 2, 3 u 4. Multiples subpreguntas pueden ser
    agregadas.
    """
    pass


class TabularQuestion(Question):

    ''' Clase base para preguntas tipo Tabla y múltiple.
    No usar directamente'''

    show_header = BooleanProperty(True)
    subq_types = ListProperty([])
    # widths of each column in proportional value (0 to 1). Always put
    # cols_widths after subq_types and cats
    cols_widths = ListProperty([])
    cols = NumericProperty(1)
    rows = NumericProperty(1)
    grid = ObjectProperty(None)
    rows_minimum = DictProperty({})

    def on_cols_widths(self, obj, value):
        if not len(self.cols_widths):
            return
        cur_col = 0
        # reverse the list of widths because kivy store childrens weirdly
        cols_w = self.cols_widths[::-1]
        for i, wd in enumerate(self.grid.children):
            wd.size_hint_x = cols_w[cur_col]
            cur_col += 1
            if (i + 1) % self.cols == 0:
                cur_col = 0

    def on_rows_minimum(self, obj, value):
        self.grid.rows_minimum = value


    @classmethod
    def get_kv_props(cls):
        properties, subqs = super(TabularQuestion, cls).get_kv_props()
        cls.cls_external_subqs_num = len(subqs)
        # Obtener subquestions definidos en subq_types (si existe)
        numcats = 1
        if 'categories' in properties:
            properties['categories'] = eval(properties['categories'].value)
            numcats = len(properties['categories'])

        if 'subq_types' in properties:
            # Environment, se necesita un root y un import subquestion
            root = Question()
            root.qid=0
            import subquestion as sq # sq es usado en eval
            # evaluar subq_types
            subq_types = eval(properties['subq_types'].value)
            properties['subq_types'] = subq_types
            # Agregar subq_types a lista de subquestions tantas veces como categorias haya
            for i in range(numcats):
                for sq_tuple in subq_types:
                    sq_cls = sq_tuple[1]
                    sq_props = {'tag': sq_tuple[0]}
                    if isinstance(sq_tuple[1], tuple):
                        sq_cls, sq_props = sq_tuple[1]
                    subqs.append((sq_cls, sq_props))

        return properties, subqs

class TableQuestion(TabularQuestion):

    """ Pregunta Tipo 1. Preguntas tipo tabla con filas categoricas fijas y
    definidas.
        subq_types define el cabezote de la tabla (titulo y subquestion class
        por cada columna).
        categories define las filas.
    """
    categories = ListProperty([])
    """ Listado de Strings que define las categorías de la pregunta"""

    def on_categories(self, obj, value):
        self.set_table()

    def set_table(self):
        """ Crea la tabla. Crea el header de la tabla, instancia las
        subpreguntas (segun subq_types)"""
        # The extra 1 is the categories column
        self.cols = len(self.subq_types) + 1
        self.rows = len(self.categories)

        # self.show_header = False
        # Add header
        if self.show_header:
            self.rows += 1  # Extra row for the header
            # TODO LABEL CSS in kv
            empty_lbl = Label(text='', color=(0, 0, 0, 1))
            empty_lbl.size_hint_x = None
            self.grid.add_widget(empty_lbl)
            for s in self.subq_types:
                text, sq = s
                # TODO LABEL CSS in kv
                self.grid.add_widget(fc.TableHeaderLabel(text=text,
                                                         color=(0, 0, 0, 1)))

        # Add rows
        # Get subquestions types to instance for each row
        stypes = zip(*self.subq_types)[1]
        stypes_text = zip(*self.subq_types)[0]
        tmp = 0
        for c in self.categories:
            # Add category
            tlbl = fc.TableLabel(text=c, )
            empty_lbl.width = max(empty_lbl.width, len(c)*6)
            self.grid.add_widget(tlbl)
            for i, s in enumerate(stypes):
                kwargs = {}
                sq_cls = s
                if isinstance(s, tuple):
                    sq_cls, kwargs = s

                tmp += 1
                # subq = sq_cls(sqid=tmp, **kwargs)
                kwargs['tag'] = stypes_text[i]
                subq = sq_cls(**kwargs)
                self.add_subq(subq)

                if isinstance(subq, ToggleField) or isinstance(subq, ListField)\
                        or isinstance(subq, CheckBoxGroupField):
                    if 'group' in kwargs:
                        kwargs['group'] += str(tmp)
                    subq.set_props(**kwargs)

                bl = AnchorLayout()
                bl.add_widget(subq)
                # we add the anchor layout that contains the subquestion
                self.grid.add_widget(bl)


class ListQuestion(TabularQuestion):

    """ Pregunta Tipo 5. Pregunta tipo tabla con filas dinámicas (se pueden
    agregar / quitar). """
    max_rows = NumericProperty(40)
    num_rows = NumericProperty(0)
    initial_rows = NumericProperty(1)
    """ Numero de filas actuales"""
    addrow_btn = ObjectProperty(None)
    """Boton Agregar Fila"""

    def load_data(self, data):
        """ El load_data es especial en este caso porque hay que agregar filas
        segun la cantidad de data """
        if data is None:
            return
        num_datarows = len(data) / len(self.subq_types)

        rows2add = num_datarows - self.num_rows

        if rows2add > 0:
            for i in range(rows2add):
                self.add_row()

        super(ListQuestion, self).load_data(data)

    @classmethod
    def cls_load_data(cls, instrument, subqs, qdata):
        # En este caso se debe repetir la cantidad de subqs segun la cantidad de filas en qdata
        #TODO OJO IMPORTANTE, subqs que es el listado de subquestions es modificado en este metodo

        outer_subqs = []
        for i in range(cls.cls_external_subqs_num):
            outer_subqs.append(subqs.pop(0))

        num_rows = 1
        if qdata is not None:
            num_rows = len(qdata) / len(subqs) #TODO ojo que en un futuro subqs puede contener subpreguntas por fuera de la tabla
            firstrow =subqs[:]
            for i in range(num_rows-1): # Se quita 1 fila que se agrega siempre
                new_row = [(x[0], dict(x[1])) for x in firstrow] 
                subqs.extend(new_row)

        outer_subqs.reverse()
        for i in outer_subqs:
            subqs.insert(0,i)
        super(ListQuestion, cls).cls_load_data(instrument, subqs, qdata)

    def on_subq_types(self, obj, value):
        self.set_table()

    def add_row(self):
        # print "Data is ", self.get_data()
        """Agrega una fila a la tabla"""
        if self.num_rows == self.max_rows:
            return
        stypes = zip(*self.subq_types)[1]
        stypes_text = zip(*self.subq_types)[0]
        tmp = 0
        # self.rows += 1
        for i, s in enumerate(stypes):
            kwargs = {}
            sq_cls = s
            if isinstance(s, tuple):
                sq_cls, kwargs = s

            tmp += 1
            # subq = sq_cls(sqid=tmp, **kwargs)
            kwargs['tag'] = stypes_text[i]
            subq = sq_cls(**kwargs)
            self.add_subq(subq)

            if isinstance(subq, ToggleField) or isinstance(subq, ListField)\
                    or isinstance(subq, CheckBoxGroupField):
                if 'group' in kwargs:
                    kwargs['group'] += str(tmp) + str(subq.sqid)
                subq.set_props(**kwargs)

            bl = AnchorLayout()
            bl.add_widget(subq)
            # we add the anchor layout that contains the subquestion
            self.grid.add_widget(bl)
        btn = fc.ButtonS(text='X')
        btn.bind(on_press=self.remove_row)
        self.grid.add_widget(btn)
        self.on_cols_widths(None, None) #Fijar widths a nuevos hijos
        self.num_rows += 1
        self._update_sqids()

        if self.num_rows > self.initial_rows: # ESto es util para actualizar la altura del qpanel 
            self.height += 40

    def remove_row(self, btn):
        """Elimina una fila de la tabla"""
        if self.num_rows == 1:
            return
        # posicion del btn en la grilla
        btn_pos = self.grid.children.index(btn)

        child_to_rem = [x for x in self.grid.children[btn_pos:btn_pos +
                                                      self.cols]]
        # Limpiar self.subquestions
        for i in child_to_rem:
            if isinstance(i, AnchorLayout):
                subq = i.children[0]
                try:
                    self.subquestions.pop(subq.sqid)
                except:
                    pass

        self.grid.clear_widgets(child_to_rem)
        self.num_rows -= 1
        self._update_sqids()

        if self.num_rows > self.initial_rows:
            self.height -= 40

    def _update_sqids(self):
        """Mantiene la numeración continua de los sqid de las subpreguntas
        (SQID-) en self.subquestions que se ve afectada cuando se agregan
        o quitan filas."""

        subquestions = [v for k, v in self.subquestions.items()]
        # resetear todo
        self.sqid_acum = 0
        self.subquestions = OrderedDict()
        for sq in subquestions:
            sq.sqid = ""
            self.add_subq(sq)

        # print "DATA IS ", self.get_data()

    def set_table(self):
        """Crea la tabla con unas filas iniciales"""
        self.cols = len(self.subq_types) + 1
        self.rows = 30
        # self.num_rows #TODO observar esto , puede ser problematico

        # self.show_header = False
        # Add header
        if self.show_header:
            # self.rows += 1  # Extra row for the header
            for s in self.subq_types:
                text, sq = s
                # TODO LABEL CSS in kv
                self.grid.add_widget(Label(text=text, color=(0, 0, 0, 1)))
            self.grid.add_widget(Label(text='', color=(0, 0, 0, 1)))

        # Add rows
        # Get subquestions types to instance for each row
        for i in range(self.initial_rows):
            self.add_row()


class MultipleQuestion(TabularQuestion):

    """ Pregunta tipo 6. Multiples preguntas tabuladas"""
    cols = NumericProperty(2)  # Always 2

    def on_subq_types(self, obj, value):
        self.set_subquestions()

    def set_subquestions(self):
        """Crea la tabla con las subpreguntas"""
        tmp = 0
        self.rows = len(self.subq_types)
        for s in self.subq_types:
            text, sq = s
            kwargs = {}
            sq_cls = sq
            if isinstance(sq, tuple):
                sq_cls, kwargs = sq

            tmp += 1
            # subq = sq_cls(sqid=tmp, **kwargs)
            kwargs['tag'] = text if 'tag' not in kwargs else kwargs['tag']
            subq = sq_cls(**kwargs)
            self.add_subq(subq)

            if isinstance(subq, ToggleField) or isinstance(subq, ListField):
                if 'group' in kwargs:
                    kwargs['group'] += str(tmp)
                subq.set_props(**kwargs)

            tlbl = fc.TableLabel(text=text)
            self.grid.add_widget(tlbl)
            bl = AnchorLayout()
            bl.add_widget(subq)
            self.grid.add_widget(bl)
