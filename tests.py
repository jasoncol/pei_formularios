import unittest
from kivy.lang import Builder
from instrument.base import Instrument
from kivy.factory import Factory

class TestQuestionPanel(unittest.TestCase):
    fname = "_test_kv.kv"

    @classmethod
    def setUpClass(cls):
        Builder.load_file("common.kv")
        from peiforms_app import PeiForms
        PeiForms.pre_init()

    def _load_kv(self, data):
        with open(self.fname, 'w') as f:
            f.write(data)
        Builder.load_file(self.fname)

    def setUp(self):
        Builder.unload_file(self.fname)

    def test_question_numbering(self):
        """Prueba que un qpanel pueda numerar correctamente las preguntas"""
        kv = """
<Question1@Question>
<Question2@Question>
<Question3@Question>
<Question4@Question>

<QPanel1@QuestionPanel>:
    questions: 'Question1', 

<QPanel2@QuestionPanel>:
    questions: 'Question2', 'Question3', 'Question4'

"""
        self._load_kv(kv)
        instru = Instrument()
        instru.questions = ['Question1', 'Question2','Question3','Question4']
        instru.qpanels = ['QPanel1', 'QPanel2']
        qp1 = Factory.QPanel1()
        qp2 = Factory.QPanel2()
        qp1.instrument = instru
        qp2.instrument = instru

        numbers = [q.number for q in qp2.get_questions()]

        self.assertEqual(numbers, [2, 3, 4])
            

    def test_question_kv_props(self):
        """Prueba que una pregunta pueda obtener propiedades definidas en el kv asi
        como sus subpreguntas"""
        kv = """
<Question1@SimpleQuestion>
    qid: 1
    text: "test"
    TextField:
        text: "test2"
    BoxLayout:
        TextField:
            text: "test3"
        """
        self._load_kv(kv)
        properties, subqs = Factory.Question1.get_kv_props()
        self.assertEqual(properties['qid'], str(1))
        self.assertEqual(subqs[0][0].__name__, 'TextField')
        self.assertEqual(subqs[0][1]['text'], 'test2')
        self.assertEqual(subqs[1][1]['text'], 'test3') #Textfield inside boxlayout

    def test_subquestion_validate_good(self):
        """Validacion de instancias de subpreguntas con dataset bueno"""
        from subquestion import TextField, NumberField, ListField, ToggleField, CheckBoxGroupField, DatePickerField, FileField
        from subquestion.base import SubQuestionException
        subqs = [TextField(), NumberField(), ListField(values=('1', '2')),
                 ToggleField(values=('1', '2')),
                 CheckBoxGroupField(values=('1', '2'), group='t2'), DatePickerField(), FileField()]

        good_dataset = ['text', 1, 1, [1, 2], ([1, 2], []), ['2015', 'Enero','1'], 'test']
        subqs[3].set_values(None, ('1', '2')) # patch
        subqs[4].set_values(None, ('1', '2'))
        for x, c in enumerate(subqs):
            c.load_data(good_dataset[x])
            try:
                c.validate()
            except Exception as e:
                print "Subquestion", c
                raise e

    def test_subquestion_validate_bad(self):
        """Validacion de instancias de subpreguntas con dataset malo"""
        from subquestion import TextField, NumberField, ListField, ToggleField, CheckBoxGroupField, DatePickerField, FileField
        from subquestion.base import SubQuestionException
        subqs = [TextField(), NumberField(), ListField(values=('1', '2')),
                 ToggleField(values=('1', '2')),
                 CheckBoxGroupField(values=('1', '2'), group='t2'), DatePickerField(), FileField()]

        subqs[3].set_values(None, ('1', '2')) # patch
        subqs[4].set_values(None, ('1', '2'))

        for x, c in enumerate(subqs):
            c.load_data(None)
            try:
                c.validate()
            except SubQuestionException:
                pass
            except Exception as e:
                print "Subquestion", c
                raise e
            else:
                raise Exception("%s no lanzo error de validacion"%(c)) 
        
    def test_question_cls_validate(self):
        """Validacion sin instanciar el widget usando metodos de clase"""
        kv="""
<QuestionValidate@SimpleQuestion>
    qid: 1
    TextField:
    NumberField:
        """
        from subquestion.base import SubQuestionException
        self._load_kv(kv)
        qdata = {"SQID-1": 'text', "SQID-2": 2}
        question_cls = getattr(Factory, "QuestionValidate")
        props, subquestions = question_cls.get_kv_props()
        #subquestions no son instancias sino tuplas de (clase_subq, props) , props obtenidas del kv
        
        instrument = Instrument()

        #Cargar data a subquestions
        question_cls.cls_load_data(instrument, subquestions, qdata)
        #Cada diccionario de propiedades (props) de cada subpregunta tiene un campo 'data' con la data correspondiente
        question_cls.cls_validate(subquestions) # No deberia lanzar excepcion
        
        #Cargar data no valida
        qdata = {"SQID-1": None, "SQID-2": None}
        question_cls.cls_load_data(instrument, subquestions, qdata)
        try:
            question_cls.cls_validate(subquestions) #Deberia lanzar excepciones de validacion
        except SubQuestionException:
            pass
        else:
            raise Exception

    def test_instrument_prevnext_navigation(self):
        kv = """
<Question1@SimpleQuestion>:
<Question2@SimpleQuestion>:
<Question3@SimpleQuestion>:
<Question4@SimpleQuestion>:
"""
        self._load_kv(kv)
        questions = ('Question1','Question2','Question3','Question4')
        instrument = Instrument()
        instrument.questions = questions
        from instrument_panel import InstrumentPanelManager
        ipm = InstrumentPanelManager()
        ipm.instrument = instrument
        ipm.questions_per_panel = 2 
        # Simular un prev para obtener el primer panel
        ipm.prev_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual

        # Verificar que contenga las preguntas correctas
        self.assertEqual(current_qpanel.questions, ['Question1', 'Question2'])

        # Simular un next
        ipm.next_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual

        # Verificar que contenga las preguntas correctas
        self.assertEqual(current_qpanel.questions, ['Question3', 'Question4'])


    def test_instrument_dependency(self):
        """Testea quelas preguntas con dependencia se muestren o no en los paneles que se van generando a medida que se navega"""
        kv = """
<Question1@SimpleQuestion>:
    qid:1
<Question2@SimpleQuestion>:
    qid:2
<Question3@SimpleQuestion>:
    qid:3
<Question4@SimpleQuestion>:
    qid:4
<Question5@SimpleQuestion>:
    qid:5
"""
        self._load_kv(kv)

        class DummyInstrument(Instrument):
            questions = ('Question1','Question2','Question3','Question4', 'Question5')
            q1_answered = False # Dummy response

            def rules_specification(self):
                self.data = {}
                self.rules = [
                        (('Question2', 'Question3'), self.testrule)
                        
                        ]
            def testrule(self):
                return self.q1_answered

        instrument = DummyInstrument()
        from instrument_panel import InstrumentPanelManager
        ipm = InstrumentPanelManager()
        ipm.instrument = instrument
        ipm.questions_per_panel = 2 

        # El primer panel debe  tener 1 y 4
        ipm.prev_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual
        self.assertEqual(current_qpanel.questions, ['Question1', 'Question4'])

        # El 2do panel debe  tener 5
        ipm.next_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual
        self.assertEqual(current_qpanel.questions, ['Question5'])


        # Simular que se respondio la primera y refrescar
        ipm.goto(0) #goto volver a la primera pregunta (primer panel)
        instrument.q1_answered = True
        ipm.refresh_panel()
        current_qpanel = ipm.curpanel # obtener panel actual
        self.assertEqual(current_qpanel.questions, ['Question1', 'Question2'])

        #Siguiente
        ipm.next_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual
        self.assertEqual(current_qpanel.questions, ['Question3', 'Question4'])

        #Siguiente
        ipm.next_panel(False)
        current_qpanel = ipm.curpanel # obtener panel actual
        self.assertEqual(current_qpanel.questions, ['Question5'])


if __name__ == '__main__':
    unittest.main()
