# -*- coding: utf-8 -*-
""" Script de arranque que inicializa la interfaz gráfica
de la App PeiForms. """

from logger import logger
from kivy.config import Config
import util  # No quitar, necesario para el deploy.
from reportlab import rl_settings

# Set window size
try:
    import pygame
    from constants import WIDTH, HEIGHT
    pygame.display.init()
    info = pygame.display.Info()
    width, height = info.current_w, info.current_h
    width, height = WIDTH, HEIGHT
    Config.set('graphics', 'width', str(width))
    Config.set('graphics', 'height', str(height - 10))
    # Config.set('graphics', 'resizable', 0)
    Config.set('graphics', 'position', 'custom')
    Config.set('graphics', 'top', 100)
    Config.set('graphics', 'left', 0)
except:
    pass

from kivy.base import Builder
Builder.load_file("common.kv")

from peiforms_app import PeiForms


if __name__ in ('__main__', '__android__'):
    logger.info("Aplicación Inicializada.")
    app = PeiForms()
    app.pre_init()
    app.run()
