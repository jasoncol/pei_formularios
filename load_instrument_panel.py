# -*- coding: utf-8 -*-
""" Este módulo contiene las deficiones del panel para cargar
un instrumento """

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.filechooser import FileChooserIconView
from kivy.clock import Clock
from kivy.properties import ObjectProperty, ListProperty
from instrument_io import InstrumentIO, InstrumentIOException
from popups import Popups
import os


class LoadInstrumentPanel(BoxLayout):

    """Panel para cargar un instrumento.
    Usa un InstrumentChooserIconView que hereda de FileChooserIconView para
    seleccionar los archivos."""

    filters = ListProperty(['*.pei'])
    """Filtros para el filechooser"""

    load_btn = ObjectProperty(None)
    delete_btn = ObjectProperty(None)

    filechooser = ObjectProperty(None)
    """Referencia a una instancia de FileChooserIconView"""

    general_instrument_panel = ObjectProperty(None)
    """Referencia al panel general del instrumento."""

    instrument_panelmgr = ObjectProperty(None)
    """Referencia al panel de diligenciamiento de preguntas"""

    send_instrument_panel = ObjectProperty(None)
    """Referencia al panel de envío  de preguntas"""

    general_tab = ObjectProperty(None)
    instrument_mgr_tab = ObjectProperty(None)
    instrument_send_tab = ObjectProperty(None)
    tabpanel = ObjectProperty(None)

    def reload(self):
        """ Actualiza el listado de archivos del filechooser."""
        print "Reloading"
        old = self.rootpath
        self.rootpath = '/'
        self.rootpath = old

    def load_instrument(self):
        """ Carga el instrumento seleccionado del filechooser y lo carga
        a los otros páneles. Se llama cuando se presiona el botón Cargar. """
        # Obtener archivo seleccionado
        f = self.filechooser.selection
        if len(f):
            f = os.path.basename(f[0])
            try:
                loaded_instrument = InstrumentIO.load_instrument(f)
            except InstrumentIOException:
                Popups.info_popup(
                    msg='Error: No se pudo cargar instrumento %s' % (f),
                    title="Error")
            else:
                # Lo carga en los otros paneles
                self.general_instrument_panel.load_instrument(
                    loaded_instrument)
                self.instrument_panelmgr.load_instrument(loaded_instrument)
                self.send_instrument_panel.instrument = loaded_instrument
                # Actualiza tabs
                self.general_tab.disabled = False
                self.instrument_mgr_tab.disabled = False
                self.instrument_send_tab.disabled = False
                self.tabpanel.switch_to(self.general_tab)

        else:
            Popups.info_popup(
                msg='Seleccione el archivo que desea cargar',
                title='No ha seleccionado ningun archivo')

    def remove_instrument(self):
        """ Presenta un popup para eliminación de archivo de instrumento.
        """
        Popups.yes_no_popup(title='Confirmar',
                            msg='¿Esta seguro que desea eliminar el archivo?',
                            callback=self._remove_instr)

    def _remove_instr(self, btn):
        """Callback para popup eliminar. Elimina el instrumento seleccionado
        del filechooser
        :param btn: boton clickeado."""

        if btn.text == Popups.YES:
            f = self.filechooser.selection
            if len(f):
                f = os.path.basename(f[0])
                InstrumentIO.remove_instrument(f)
                self.reload()


class InstrumentChooserIconView(FileChooserIconView):

    """FilechooserIconView personalizado. Usa FileIconEntry2 definido
    en load_instrument_panel.kv"""
    _ENTRY_TEMPLATE = 'FileIconEntry2'

    def __init__(self, *args, **kwargs):
        super(InstrumentChooserIconView, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._update_scrollbar)

    def on_layout(self, obj, value):
        self.layout._ENTRY_TEMPLATE = self._ENTRY_TEMPLATE

    def _update_scrollbar(self, t):
        """Personaliza el scrollview de este widget"""
        # TODO reglas de estilo, no es deseable que vayan aqui
        layout = self
        try:
            layout = self.layout # self in kivy 1.8, self.layout
        except:
            pass
        print "ids", layout.ids
        if 'scrollview' in layout.ids:
            sbar = layout.ids['scrollview']
            sbar.scroll_type = ['bars']
            sbar.bar_width = 15
    
