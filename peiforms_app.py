# -*- coding: utf-8 -*-
""" Este módulo define la aplicación principal (PeiForms) y el widget principal
contenedor (MainPanel). """

from logger import logger
from kivy.base import Builder
from kivy.app import App
from kivy.factory import Factory
from kivy.uix.boxlayout import BoxLayout
import os
from question import SimpleQuestion, TableQuestion, MultipleQuestion
from question_panel import QuestionPanel
from instrument_panel import InstrumentPanelManager
from subquestion import (TextField, TextArea, NumberField, ListField,
                         ToggleField, BaseTextInput, CheckBoxButton,
                         CheckBoxGroupField, ClickLabel, FileField,
                         DepartamentoToggleField,
                         SubregionToggleField, MunicipioToggleField,
                         DatePickerField)
from initial_panel import InitialPanel
from load_instrument_panel import (LoadInstrumentPanel,
                                   InstrumentChooserIconView)
from general_instrument_panel import GeneralInstrumentPanel
from send_instrument_panel import SendInstrumentPanel
from instrument.instrumento_impacto import InstrumentoImpacto
from instrument.instrument2 import Instrument2


class MainPanel(BoxLayout):

    """ Widget principal que contiene todos los widgets de la UI. Sus reglas
    estan definidas en peiforms.kv """
    rootpath = os.path.dirname(__file__)


class PeiForms(App):

    """ App principal del sistema """
    use_kivy_settings = False


    with open("VERSION.txt", 'r') as f:
        version = f.read().strip()
        title = ("PEI: Aplicación para captura la de información"
                 "(Versión: %s)" % (version))

    PROFILE = False

    def build_config(self, config):
        config.setdefaults('pei', {
            'url': 'http://45.55.138.14:8000',
        })

    def build_settings(self, settings):
        jsondata = """[
         { "type": "title",
      "title": "Configuraciones" },
        { "type": "string",
      "title": "URL servidor de reportes",
      "desc": "Dirección del servidor de reportes",
      "section": "pei",
      "key": "url" }
        ]"""
        settings.add_json_panel('PEI',
            self.config, data=jsondata)

    def build(self):
        return MainPanel()

    @classmethod
    def pre_init(cls):
        """ Inicializa los componentes de la GUI. Carga los archivos .kv y
        registra las clases de los widgets.
        """
        # Register classes
        widg_classes = [('MainPanel', MainPanel),
                        ('BaseTextInput', BaseTextInput),
                        ('TextField', TextField), ('TextArea', TextArea),
                        ('NumberField', NumberField), ('ListField', ListField),
                        ('ToggleField', ToggleField), ('CheckBoxGroupField',
                                                       CheckBoxGroupField),
                        ('CheckBoxButton',
                         CheckBoxButton), ('SimpleQuestion', SimpleQuestion),
                        ('TableQuestion', TableQuestion), ('MultipleQuestion',
                                                           MultipleQuestion),
                        ('QuestionPanel',
                         QuestionPanel), ('ClickLabel', ClickLabel),
                        ('InstrumentPanelManager', InstrumentPanelManager),
                        ('LoadInstrumentPanel', LoadInstrumentPanel),
                        ('InstrumentChooserIconView',
                         InstrumentChooserIconView),
                        ('GeneralInstrumentPanel', GeneralInstrumentPanel),
                        ('InitialPanel', InitialPanel), ('SendInstrumentPanel',
                                                         SendInstrumentPanel),
                        ('FileField', FileField),
                        ('SubregionToggleField', SubregionToggleField),
                        ('DepartamentoToggleField', DepartamentoToggleField),
                        ('MunicipioToggleField', MunicipioToggleField),
                        ('DatePickerField', DatePickerField)]

        logger.info("Registrando widgets en kv")
        for s in widg_classes:
            Factory.register(*s)

        # Load additional kv rules
        Builder.load_file("subquestion.kv")
        Builder.load_file("question.kv")

        # Load questions
        rootp = os.path.dirname(__file__)

        # Instruments question panels dictionaries
        logger.info("Cargando qpanels de instrumentos")
        try:
            InstrumentoImpacto._load_qpanels('impacto.kv')
            Instrument2._load_qpanels('i2.kv')
        except Exception as e:
            logger.error("Error cargando qpanels", exc_info=True)

        logger.info("Cargando kv de secciones")
        Builder.load_file("initial_panel.kv")
        Builder.load_file("general_instrument_panel.kv")
        Builder.load_file("load_instrument_panel.kv")
        Builder.load_file("send_instrument_panel.kv")

    def load_instrument_kv(self, instrument_cls):
        """ Carga las reglas kv (preguntas, paneles). del instrumento
        especificado (Clase del instrumento).

        :param instrument_cls: Clase del instrumento al cual se le quieren
        cargar las reglas kv.

        :returns: None
        """

        print "Loading rules for instrument", instrument_cls
        kv_dict = {InstrumentoImpacto: 'impacto', Instrument2: 'i2'}
        si = kv_dict[instrument_cls]
        # Unload everything first
        rootp = os.path.dirname(__file__)
        for i, v in kv_dict.iteritems():
            print "unloading", v
            Builder.unload_file(os.path.join(rootp, "kv/panels/%s.kv" % (v)))
            for kv in os.listdir(os.path.join(rootp, 'kv/questions/%s' % (v))):
                if kv[-3:] == '.kv':
                    Builder.unload_file(
                        os.path.join(rootp, "kv/questions/%s/%s" % (v, kv)))

        # Load Instrument rules
        for kv in os.listdir(os.path.join(rootp, 'kv/questions/%s' % (si))):
            if kv[-3:] == '.kv':
                Builder.load_file(
                    os.path.join(rootp, "kv/questions/%s/%s" % (si, kv)))

        print "loading", si
        Builder.load_file(os.path.join(rootp, "kv/panels/%s.kv" % (si)))

    def on_start(self):
        if PeiForms.PROFILE:
            import cProfile
            self.profile = cProfile.Profile()
            self.profile.enable()

    def on_stop(self):
        if PeiForms.PROFILE:
            self.profile.disable()
            self.profile.dump_stats('peiforms.profile')
