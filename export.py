# -*- coding: latin-1 -*-
"""Modulo para exportacion del instrumento a otros formatos"""

from reportlab.lib.pagesizes import A4, cm
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from kivy.factory import Factory
from question import (SimpleQuestion, TableQuestion, ListQuestion,
                      MultipleQuestion)
import time

class PDFExporter(object):

    """Clase para exportar a pdf"""

    def export(self, instrument, fname=None):
        """Exporta a pdf el instrumento
        :param fname: ruta y/o nombre del fichero pdf. Si no se especifica se usa reporte_[timestamp].pdf.

        :returns: ruta del archivo generado.
        """
        ktime = time.time()
        if fname is None:
            fname = "reporte_%s.pdf"%(time.time())
        self.instrument = instrument
        width, height = A4
        doc = SimpleDocTemplate(fname,
                                pagesize=A4, rightMargin=18, leftMargin=18,
                                topMargin=18, bottomMargin=18)
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='answer', textColor=colors.blue))
        self.styles = styles
        Story = []
        # Agregar imagen de header
        Story.append(Image("./img/header_pdf.png", 20 * cm))
        # Agregar datos generales de instrumento
        Story.append(Spacer(1, 12))
        Story.append(Spacer(1, 12))
        Story.append(
            Paragraph("<b>Datos generales del instrumento</b>",
                      styles['Normal']))
        Story.append(self.general_data())
        Story.append(Spacer(1, 12))
        Story.append(Spacer(1, 12))

        # Respuestas a preguntas
        Story.append(Paragraph("<b>Respuestas</b>", styles['Normal']))
        curnum = 0
        # Ningun widget se instancia. Se accede a las funcionalidades e informacion 
        # usando los métodos de clase disponibles

        for question_name in self.instrument.questions:

            question_cls = getattr(Factory, question_name)
            if not self.instrument.execute_rules(question_name):
                pass
                #continue # TODO

            props, subquestions = question_cls.get_kv_props()
            curnum += 1
            print "QUESTION", curnum, props['text']
            question_clsobj = props['clsobj']
            qid = props['qid']
            qdata = self.instrument.get_question_data(qid)

            question_cls.cls_load_data(self.instrument, subquestions, qdata) # Cargar qdata a subquestions

                
            Story.append(
                Paragraph("<b>%s.</b> %s" % (curnum,
                                             self.format_text(props['text'])),
                          styles['Normal']))
            Story.append(Spacer(1, 12))
            if  question_clsobj is SimpleQuestion:
                for sq in subquestions:
                    sq_cls, sq_props = sq
                    try:
                        Story.append(
                            Paragraph("%s" % (sq_cls.get_data_str(**sq_props)),
                                      styles['answer']))
                    except Exception as e:
                        print e

            elif question_clsobj is MultipleQuestion:
                col_labels = zip(*props['subq_types'])[0]
                data = []
                for i, label in enumerate(col_labels):
                    sq_cls, sq_props = subquestions[i]

                    label = Paragraph(
                        "<b>%s</b>" % (label), styles['BodyText'])
                    data.append([label,
                                 "%s" % (sq_cls.get_data_str(**sq_props))])

                table = self.create_table(data, firstrow_index=0)
                Story.append(table)

            elif question_clsobj is TableQuestion:

                #---Fix external  optional subq
                outer_subqs = []
                for i in range(question_cls.cls_external_subqs_num):
                    outer_subqs.append(subquestions.pop(0))
                    #TODO append question to end of table
                #End fix

                row_labels = list(zip(*props['subq_types'])[0])
                col_labels = props['categories']
                data = []
                # Agregar primera fila
                data.append(
                    [Paragraph("<b>%s</b>" % (x), styles['BodyText'])
                     for x in [''] + row_labels])
                cnt = 0
                # Agregar resto de filas
                for i, label in enumerate(col_labels):
                    label = Paragraph(
                        "<b>%s</b>" % (label), styles['BodyText'])
                    row = [label]
                    for j in range(len(row_labels)):
                        sq_cls, sq_props = subquestions[cnt]
                        cnt += 1
                        row.append( Paragraph(
                            "%s" % (sq_cls.get_data_str(**sq_props)),  styles['answer']))
                    data.append(row)
                table = self.create_table(data)
                Story.append(table)
                if len(outer_subqs):
                    #TODO solo agrega la primera subq, agregar otras
                   sq_cls, sq_props = outer_subqs[0]
                   Story.append( Paragraph(
                            "%s" % (sq_cls.get_data_str(**sq_props)),  styles['answer']))


            elif question_clsobj is ListQuestion:
                #---Fix external  optional subq
                outer_subqs = []
                for i in range(question_cls.cls_external_subqs_num):
                    outer_subqs.append(subquestions.pop(0))
                    #TODO append question to end of table
                #End fix

                row_labels = list(zip(*props['subq_types'])[0])
                data = []
                # Agregar primera fila
                data.append(
                    [Paragraph("<b>%s</b>" % (x), styles['BodyText'])
                     for x in [''] + row_labels])
                
                cnt = 0
                num_rows = 1
                if qdata is not None:
                    num_rows = len(qdata) / len(row_labels)

                # Agregar resto de filas
                for i in range(num_rows):
                    label = Paragraph(
                        "<b>%s.</b>" % (i + 1), styles['BodyText'])
                    row = [label]
                    for j in range(len(row_labels)):
                        sq_cls, sq_props = subquestions[cnt]
                        cnt += 1
                        row.append(Paragraph(
                            "%s" % (sq_cls.get_data_str(**sq_props)),  styles['answer']))
                    data.append(row)
                table = self.create_table(data)
                Story.append(table)
                if len(outer_subqs):
                    #TODO solo agrega la primera subq, agregar otras
                   sq_cls, sq_props = outer_subqs[0]
                   Story.append( Paragraph(
                            "%s" % (sq_cls.get_data_str(**sq_props)),  styles['answer']))

            Story.append(Spacer(1, 12))
            Story.append(Spacer(1, 12))

        doc.build(Story)
        print "FINALIZADO ET:", time.time() - ktime

        
        return fname


    def general_data(self):
        """Crea el cuadro de datos generales del reporte de instrumento"""
        import time
        today = time.strftime("%d/%m/%Y %H:%M:%S")
        type_str = {1: 'Construcción completa: Son los casos donde no existe Institución educativa y se va a construir desde cero.', 2: 'Reposición completa: Se hace una demolición total de las ediﬁcaciones que existen en el lote, para hacer una \nnueva construcción con las especiﬁcaciones adecuadas y bajo las normas establecidas.', 3: 'Reposición parcial: Se hace una demolición de las ediﬁcaciones que presentan problemas (ya sea espaciales o estructurales),\npara hacer una nueva construcción con las especiﬁcaciones adecuadas y bajo las normas establecidas.', 4: 'Ampliación de espacios básicos: Construcción de nuevas aulas requeridas de acuerdo al número de estudiantes en la institución educativa.', 5: 'Ampliación de espacios complementarios: Construcción de nuevos espacios complementarios, teniendo en cuenta los que le hacen falta a la institución educativa.',6: 'Mejoramiento de los espacios: Se hacen intervenciones para mejorar la ediﬁcación: pintura, ventanería, puertas, baterías sanitarias, red eléctrica.'}

        data = [
            [Paragraph("<b>Fecha de generación</b>", self.styles['BodyText']),
             Paragraph(today, self.styles['answer'])],
            [Paragraph("<b>Nombre del registro del instrumento</b>",
                       self.styles['BodyText']),
             Paragraph(self.instrument.name, self.styles['answer'])],
            [Paragraph("<b>Fecha de inicio de intervención</b>", self.styles['BodyText']),
             Paragraph(self.instrument.construction_start_date, self.styles['answer'])],
            [Paragraph("<b>Fecha de finalización de intervención</b>", self.styles['BodyText']),
             Paragraph(self.instrument.construction_finish_date, self.styles['answer'])],
            [Paragraph("<b>Tipo de intervención</b>",
                       self.styles['BodyText']),
             Paragraph(self.format_text(type_str[self.instrument.intervention_type[0]]),
                       self.styles['answer'])],
            [Paragraph("<b>Fecha de los datos</b>", self.styles['BodyText']),
             Paragraph(self.instrument.date, self.styles['answer'])],
            [Paragraph("<b>Nombre del encuestador</b>",
                       self.styles['BodyText']),
             Paragraph(self.instrument.submitter_name, self.styles['answer'])],

        ]
        return self.create_table(data, firstrow_index=0)

    def create_table(self, data, firstrow_index=1):
        """Crea una tabla y usando la informacion en data
        :param firstrow_index: indice de la primera fila donde estan las
        respuestas en la tabla. A veces es la primera fila (0)
        o la segunda (1)"""

        num_cols = len(data[0])
        colWidths = None
        if num_cols > 2:
            col1_width = [4 * cm] if len(data[1][0].text) > 10 else [1 * cm]
            colWidths = col1_width + [3 * cm] * (num_cols - 1)

        if num_cols >= 7:
            colWidths = col1_width + [2 * cm] * (num_cols - 1)

        t = Table(data, hAlign='LEFT', colWidths=colWidths)
        t.setStyle(TableStyle([('ALIGN', (0, 1), (-1, -1), 'LEFT'),
                               ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                               ('INNERGRID', (0, 0),
                                (-1, -1), 0.25, colors.black),
                               ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                               ('TEXTCOLOR', (1, firstrow_index),
                                (-1, -1), colors.blue)
                               ]))
        return t

    def format_text(self, text):
        text_formatted = text.replace('\n', '<br/>')
        return text_formatted
