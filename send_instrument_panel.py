# -*- coding: utf-8 -*-
""" Este módulo contiene las deficiones del panel de envío del instrumento."""
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.factory import Factory
import time
from kivy.properties import (ObjectProperty, StringProperty,
                             NumericProperty, ListProperty)
from kivy.clock import Clock, mainthread
from functools import partial
import requests
from threading import Thread
from logger import logger
import hashlib
import json
from export import PDFExporter
import os
from subquestion.base import SubQuestionException
from popups import Popups
# Cantidad maxima de errores a chequear. una vez superado no se continua
# la validación.
MAX_ERRORS = 50
MAX_ERRORS_DISPLAY = 10


class SendInstrumentPanel(BoxLayout):

    """ Panel para valiadr y enviar instrumento. """
    instrument = ObjectProperty(None)
    error_cont = ObjectProperty(None)
    pbar = ObjectProperty(None)
    pbar2 = ObjectProperty(None)
    error_lbl = ObjectProperty(None)
    error_lbl2 = ObjectProperty(None)
    pbar_value = NumericProperty(0)
    instrument_mgr_tab = ObjectProperty(None)
    tabpanel = ObjectProperty(None)
    instrument_panelmgr = ObjectProperty(None)
    """Referencia al panel de diligenciamiento de preguntas"""
    login_question = ObjectProperty(None)
    send_btn = ObjectProperty(None)

    URL_SITE = 'http://localhost:8000'  # 'http://localhost:8000'
    URL_POST = '/instrumento/upload'
    send_status = ListProperty(['', 0])
    
    def __init__(self, *args, **kwargs):
        super(SendInstrumentPanel, self).__init__(*args, **kwargs)
        from kivy.app import App
        self.URL_SITE = App.get_running_app().config.get('pei', 'url')
        self.URL_POST = self.URL_SITE + self.URL_POST

    def send_instrument(self):
        """Envía el instrumento"""
        self.instrument.validated = True  # TODO temporal
        self.send_btn.disabled = True
        self.pbar_value = 0
        self.si_thread = Thread(target=self._send_instrument_thread)
        self.si_thread.daemon = True
        self.si_thread.setDaemon(1)
        self.si_thread.start()

    def _send_instrument_thread(self):
        sent = False
        try:
            if not self.instrument.validated:
                raise InstrumentNotValidated()
                pass
            user = self.login_question.get_subq(
                tag='Nombre de usuario').get_data()
            pass_ = self.login_question.get_subq(tag='Contraseña').get_data()

            if not len(user) or not len(pass_):
                raise IncorrectLogin()
            self.send_status[0] = "Preparando envío..."
            self.instrument.save_data()
            data = {}

            data['user'] = user
            data['passw'] = pass_
            with open("./data/" + self.instrument.fname, 'rb') as f:
                md5sum = hashlib.md5()
                md5sum.update(f.read())
                data['md5sum'] = md5sum.digest()

            
            # Hacer un ping antes de enviar al servidor
            self.send_status[0] = "Contactando servidor PEI ..."
            self.send_status[1] = 35
            try:
                r = requests.get(self.URL_SITE, timeout=10)
            except:
                raise ServerUnreachableError()
            
            # Reporte pdf
            self.send_status[0] = "Generando reporte de instrumento ..."
            self.send_status[1] = 10
            pdf_fname = self.export()

            # Archivos a enviar
            files = [
                ('instrument', open("./data/" + self.instrument.fname, 'rb')),
                ('report', open(pdf_fname, 'rb'))
            ]


            # Si no hubo error continuar con el post
            self.send_status[0] = "Enviando instrumento. Por favor espere ..."
            self.send_status[1] = 40
            try:
                r = requests.post(self.URL_POST, data=data, files=files)
            except:
                raise ServerUnreachableError()

            print "response is", r.text
            response = json.loads(r.text)
            if 'error' in response:
                raise ServerProcessError(response['error'])
            print "Response is ", response
            self.send_status[0] = ("El instrumento Ha sido enviado"
                                   " exitósamente al servidor PEI")
            self.send_status[1] = 100
            sent = True
        except InstrumentNotValidated:
            self.send_status[0] = ("Error: El instrumento no ha superado la validación."
                                   " Haga click en validar.")
        except IncorrectLogin:
            self.send_status[0] = ("Error: Datos de acceso incorrectos,"
                                   " por favor verificarlos.")
        except ServerProcessError as e:
            self.send_status[0] = ("Error en el procesamiento del servidor remoto: %s" % (e))
        except ServerUnreachableError:
            self.send_status[0] = "Error. No se pudo conectar al servidor remoto"
        except Exception as e:
            self.send_status[0] = ("Error accediendo al servidor PEI."
                                   " Por favor intente más tarde.")
            logger.error("No se pudo acceder al servidor.", exc_info=True)

        # Limpiar y devolver todo al estado anterior
        self.send_btn.disabled = False
        try:
            #os.remove(pdf_fname) # TODO
            pass
        except:
            pass
        if  sent:
            Popups.info_popup(
                msg='El instrumento ha sido enviado al servidor de reportes para su verificación. Muchas gracias',
                title='Instrumento enviado exitosamente')

    @mainthread
    def on_send_status(self, obj, value):
        self.error_lbl2.text = value[0]
        self.pbar2.value = value[1]

    def validate_instrument(self):
        """Valida el instrumento"""
        self.error_cont.clear_widgets()
        self.pbar.value = 0
        self.error_msgs = []
        self.error_lbl.text = "Validando. Por favor espere..."
        self.cnt = 0
        self.tot_panels = len(self.instrument.qpanels)
        self.cur_question = 0
        self.instrument.validated = False
        #self.cl = Clock.schedule_interval(self._validate_questions, 0.05)
        self.qcnt = 0
        self.ktime = time.time()
        self._validate_questions()

    def _validate_questions(self, t=0):
        """Valida todas las preguntas del instrumento."""

        if self.cnt >= MAX_ERRORS:
            return

        if self.cur_question == len(self.instrument.questions):
            # Validar si se recorrieron todos los questions y no hay errores
            print "Fin validacion",t
            if self.cnt == 0:
                self.instrument.validated = True
            self.pbar.value = 100
            return self.show_errors(self.error_msgs)


        question_name = self.instrument.questions[self.cur_question]
        print "question name", question_name

        if not self.instrument.execute_rules(question_name):
            self.cur_question += 1
            self.qcnt += 1 
            Clock.schedule_once(self._validate_questions)
            return 


        question_cls = getattr(Factory, question_name)

        self.qcnt += 1
        if self.cnt == MAX_ERRORS:
            self.pbar.value = 100
            self.cur_question += 1
            return self.show_errors(self.error_msgs)
        try:
            props, subquestions = question_cls.get_kv_props()
            qdata = self.instrument.get_question_data(props['qid'])
            question_cls.cls_load_data(self.instrument, subquestions, qdata)
            question_cls.cls_validate(subquestions) # No deberia lanzar excepcion
        except SubQuestionException as e:
            self.error_msgs.append(
                ("Pregunta %s: %s" % (self.qcnt, e.value),
                 question_name))
            self.cnt += 1
        except Exception as e:
            print question_cls
            print e
        self.pbar.value = self.cnt * 100. / MAX_ERRORS

        self.cur_question += 1
        Clock.schedule_once(self._validate_questions)

    def show_errors(self, error_msgs):
        print "ET", time.time() - self.ktime
        #self.cl.cancel()
        if len(error_msgs):
            self.error_lbl.text = "Resultados de validacion: %s+ errores" % (
                len(error_msgs))
            for i, e in enumerate(error_msgs):
                if i > MAX_ERRORS_DISPLAY:
                    return
                btn = Factory.ButtonValidation(text=e[0])
                btn.bind(on_press=partial(self._show_instr, e[1]))
                self.error_cont.add_widget(btn)
        else:
            pass

    def _show_instr(self, question_cls, btn):
        self.tabpanel.switch_to(self.instrument_mgr_tab)
        question_pos = self.instrument.questions.index(question_cls)
        self.instrument_panelmgr.goto(question_pos) #TODO el goto muestra desde la pregunta en cuestion
        # Esto puede generar confusion al usuario pues esa pregunta pudo no haber estado de primera en un panel
        # previamente

    def export(self):
        """Exporta a pdf"""
        pe = PDFExporter()
        fname = "temp/reporte_%s.pdf"%(time.time())
        fname = pe.export(self.instrument, fname)
        return fname

class IncorrectLogin(Exception):
    pass


class InstrumentNotValidated(Exception):
    pass


class ServerProcessError(Exception):
    pass

class ServerUnreachableError(Exception):
    pass
