# -*- coding: latin-1 -*-
"""Instrumento Tipo 2"""
from base import Instrument, INSTRUMENT_TYPE_2
import constants as ct
from collections import OrderedDict

VERSION = '0.1'
VERSION_DATE = '02/12/2014'

class Instrument2(Instrument):
    """Instrumento Tipo 2. Ver clase Instrument (instrument.base.Instrument) para más info sobre los métodos y atributos."""

    instrument_type = INSTRUMENT_TYPE_2
    instrument_version, instrument_versiondate = VERSION, VERSION_DATE
    sections = OrderedDict([("Identificacion de la Institucion Educativa (1 a 2)", "QPanel1"),
        ("Talento Humano (3 a 8)", "QPanel2"),
        ("Capacitacion Docente (9 a 14)", "QPanel4"),
        ("Rotacion de personal (15 a 18)", "QPanel7"),
        ("Logros educativos (19 a 23)", "QPanel8"),
        ("Participacion (24)", "QPanel10"),
        ("Libros y documentos reglamentarios (25)", "QPanel11"),
        ("PEI (26 a 33)", "QPanel12"),
        ("Calidad educativa: Desercion (34 a 37)", "QPanel17"),
        ("Calidad educativa: Ausentismo (38 a 46)", "QPanel21"),
        ("Calidad educativa: Retencion (47 a 48)", "QPanel26"),
        ("Calidad educativa: Repitencia (49 a 51)", "QPanel27"),
        ("Calidad educativa: Promocion (52 a 54)", "QPanel28"),
        ("Convivencia (55)", "QPanel29"),

    ])

    def rules_specification(self):
        self.rules = {
            'QPanel201': self._mostrar_preescolar_rule,
            'QPanel6': self._mostrar_preescolar_rule,
            'QPanel7': self._mostrar_preescolar_rule,
            'QPanel17': self._mostrar_preescolar_rule,
            'QPanel22': self._mostrar_ausentismo_preescolar,
            'QPanel3': self._mostrar_primaria_rule,
            'QPanel601': self._mostrar_primaria_rule,
            'QPanel701': self._mostrar_primaria_rule,
            'QPanel8': self._mostrar_primaria_rule,
            'QPanel18': self._mostrar_primaria_rule,
            'QPanel23': self._mostrar_ausentismo_primaria,
            'QPanel301': self._mostrar_secundaria_rule,
            'QPanel602': self._mostrar_secundaria_rule,
            'QPanel702': self._mostrar_secundaria_rule,
            'QPanel801': self._mostrar_secundaria_rule,
            'QPanel19': self._mostrar_secundaria_rule,
            'QPanel24': self._mostrar_ausentismo_secundaria,
            'QPanel302': self._mostrar_media_rule,
            'QPanel603': self._mostrar_media_rule,
            'QPanel703': self._mostrar_media_rule,
            'QPanel802': self._mostrar_media_rule,
            'QPanel20': self._mostrar_media_rule,
            'QPanel25': self._mostrar_ausentismo_media,
            'QPanel5': self._mostrar_enfoques_rule,
            'QPanel15': self._mostrar_bibliotecas_rule,
        }

    def __init__(self, *args, **kwargs):
        super(Instrument2, self).__init__(*args, **kwargs)

    #Grado rules
    Q2_ID, SQ2_ID = 2, 1

    def _mostrar_grado_rule(self, grado):
        """ generic rule to show panels depending on grado """
        qdata = self.get_question_data(Instrument2.Q2_ID)
        sqdata = self.get_subq_data(qdata, Instrument2.SQ2_ID)
        if sqdata is None:
            return False
        if grado in sqdata:
            return True
        return False

    def _mostrar_preescolar_rule(self):
        return self._mostrar_grado_rule(ct.PREESCOLAR)

    def _mostrar_primaria_rule(self):
        return self._mostrar_grado_rule(ct.BASICA_PRIMARIA)

    def _mostrar_secundaria_rule(self):
        return self._mostrar_grado_rule(ct.BASICA_SECUNDARIA)

    def _mostrar_media_rule(self):
        return self._mostrar_grado_rule(ct.MEDIA)

    # Rules for ausentismo
    Q38_ID, SQ38_ID = 38, 1

    def _mostrar_ausentismo_rule(self, grado, ausentismo):
        """ generic rule to show panels depending on grado and ausentismo"""
        qdata2 = self.get_question_data(Instrument2.Q2_ID)
        qdata38 = self.get_question_data(Instrument2.Q38_ID)
        sqdata2 = self.get_subq_data(qdata2, Instrument2.SQ2_ID)
        sqdata38 = self.get_subq_data(qdata38, Instrument2.SQ38_ID)
        if sqdata2 is None or sqdata38 is None:
            return False
        if grado in sqdata2 and ausentismo in sqdata38:
            return True
        return False

    def _mostrar_ausentismo_preescolar(self):
        return self._mostrar_ausentismo_rule(ct.PREESCOLAR, 1)

    def _mostrar_ausentismo_primaria(self):
        return self._mostrar_ausentismo_rule(ct.BASICA_PRIMARIA, 1)

    def _mostrar_ausentismo_secundaria(self):
        return self._mostrar_ausentismo_rule(ct.BASICA_SECUNDARIA, 1)

    def _mostrar_ausentismo_media(self):
        return self._mostrar_ausentismo_rule(ct.MEDIA, 1)

    # Generic yes-no that shows other panel(s)

    def _mostrar_yesno_rule(self, q_id, sq_id):
        """ generic rule to show panels depending on some yes-no question"""
        qdata = self.get_question_data(q_id)
        sqdata = self.get_subq_data(qdata, sq_id)
        if sqdata is None:
            return False
        if 1 in sqdata:
            return True
        return False

    # Rule for programas de formacion q9
    Q9_ID, SQ9_ID = 9, 1

    def _mostrar_enfoques_rule(self):
        return self._mostrar_yesno_rule(Instrument2.Q9_ID, Instrument2.SQ9_ID)

    # Rule for biblioteca q30
    Q30_ID, SQ30_ID = 30, 1

    def _mostrar_bibliotecas_rule(self):
        return self._mostrar_yesno_rule(Instrument2.Q30_ID, Instrument2.SQ30_ID)
