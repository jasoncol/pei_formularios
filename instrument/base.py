# -*- coding: latin-1 -*-
""" Este módulo contiene las deficiones de la clase padre de todos los tipos de
instrumentos."""


import os
import re
from instrument_io import InstrumentIO
from collections import OrderedDict
from kivy.event import EventDispatcher
from kivy.properties import StringProperty

INSTRUMENT_TYPE_1 = '1'
INSTRUMENT_TYPE_2 = '2'


class Instrument(EventDispatcher):

    """Clase base de la que heredan todos los tipos de instrumentos."""

    data = {}
    """Diccionario con la información que se diligencia del instrumento
     (respuestas a las preguntas). Usa como llaves (keys) los id de las
     preguntas (y sus subpreguntas) del instrumento."""

    qpanels = []
    """Listado de QuestionPanel existentes para el instrumento"""

    questions = []
    """Listado de preguntas"""

    question_panels_index = []
    """Contiene listado sobre a que qpanel pertenece cada pregunta en questions"""

    rules = {}
    """Reglas de navegación del instrumento"""
    name, date, institution, = StringProperty(''), StringProperty(''), StringProperty('')

    construction_start_date, construction_finish_date = StringProperty(''), StringProperty('')
    """ Fechas de inicio y entrega de la intervención """

    instrument_type = ''
    """Tipo de instrumento. Identificador."""

    instrument_version, instrument_versiondate = '', ''
    submitter_name = ''
    root_p = ''

    intervention_type = 0
    ''' Tipo de intervención que se realizó en la Institución encuestada'''

    observations = ''
    ''' Observaciones generales sobre la intervención'''

    fname = StringProperty('')
    """Filename. Nombre del archivo donde se almacena el instrumento"""

    sections = {}
    """Subsecciones del instrumento"""

    validated = False
    """True si el instrumento ha sido validado"""

    def __init__(self, *args, **kwargs):
        super(Instrument, self).__init__(*args, **kwargs)
        self.data = {}
        self.rules_specification()

    @classmethod
    def _load_qpanels(cls, fname):
        """ Carga los nombres de los QuestionPanel definidos en
        kv/panels/[fname] en el mismo orden en que aparecen. El listado se
        almacena en instrument.qpanels.
        Asi mismo se obtienen las preguntas de los qpanels y los nombres de
        las clases se guardan en instrument.questions y se cruza en
        instrument.questions_panels_index

       :param fname: Ruta del archivo kv que especifica los QuestionPanel.
       """
        '''
        cls.qpanels = []
        cls.questions = []
        cls.question_panels_index = []

        clsname = 'QuestionPanel'
        p = re.compile('\w+@%s' % (clsname), re.IGNORECASE)
        q = re.compile('questions:\s.+', re.IGNORECASE)
        with open(os.path.join(Instrument.root_p,
                               "kv/panels/%s" % (fname)), 'r') as f:
            s = f.read()
            l = [x[:-len(clsname) - 1] for x in p.findall(s)]
            questions = [x[12:len(x) - 1] for x in q.findall(s)]
            cls.qpanels = l

            for i, x in enumerate(cls.qpanels):
                qlist = ['Question%s' % (xq.strip())
                         for xq in questions[i].split(',') if len(xq.strip())]
                print "qpanel", x, "qlist", qlist
                cls.questions.extend(qlist)
                for xq in qlist:
                    cls.question_panels_index.append(i)
        '''
        pass # TODO
    def save_data(self):
        """Guarda la información del instrumento en disco"""
        if self.fname == '':
            print "Invalid fname"
            return
        InstrumentIO.save_instrument(self, self.fname)

    def set_question_data(self, question):
        """Actualiza la información de la pregunta del instrumento en self.data
        :param question: Instancia Question que tiene el input del usuario.
        """
        qdata = question.get_data()
        self.data["Q%s" % (question.qid)] = qdata
        # print "Data instrument", self.data
        self.validated = False

    def get_question_data(self, qid):
        """ Obtiene la información de la pregunta del instrumento almacenada en
        self.data.

        :param qid: Id de la pregunta.
        :returns: data de la pregunta (diccionario).
        """

        if "Q%s" % (qid) in self.data:
            return self.data["Q%s" % (qid)]
        return None  # TODO exception ? noquestiondatafound

    def get_subq_data(self, data, sqid):
        """ Devuelve data de Subquestion con id sqid en diccionario de entrada
        data
        :param data: información (diccionario).
        :param sqid: id de subpregunta.
        :returns: data de subpregunta (puede ser texto, numero o list) o None.
        """
        key = "SQID-%s" % (sqid) if 'SQID' not in str(sqid) else str(sqid)
        if data is None:
            return None
        if key in data:
            return data[key]
        return None

    def rules_specification(self):
        """Este método debe ser usado por clases que heredan de esta clase para
        especificar las reglas de navegación del instrumento. """
        self.rules = {}

    def execute_rules(self, question_name):
        """Determina si question_name (Question) debe ser mostrado según las
        reglas de navegación

        :param question_name: Nombre del Question del instrumento.
        :returns: True o False dependiendo si se debe mostrar  o no.
        """

        for rule in self.rules:
            qs, fun = rule
            if question_name in qs:
                if self.data is None:
                    return False
                return fun()  

        return True

    def validate_header(self):
        """Valida los datos del header del instrumento"""
        # TODO
