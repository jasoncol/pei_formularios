# -*- coding: latin-1 -*-
"""Instrumento Impacto"""
from base import Instrument, INSTRUMENT_TYPE_1
import constants as ct
from collections import OrderedDict

VERSION = '0.4'
VERSION_DATE = '26/10/2015'


class InstrumentoImpacto(Instrument):
    """Instrumento Impacto. Ver clase Instrument (instrument.base.Instrument) para m�s info sobre los m�todos y atributos."""

    instrument_type = INSTRUMENT_TYPE_1
    instrument_version, instrument_versiondate = VERSION, VERSION_DATE

    sections = OrderedDict([("Informaci�n general del Establecimiento Educativo", "Question1"),
                            ("Integraci�n de la infraestructura f�sica al proceso de ense�anza y aprendizaje",
                             "Question9"),
                            ("Infraestructura f�sica y calidad de vida y del empleo de los docentes", "Question37"),
                            ("Infraestructura f�sica y participaci�n y desarrollo de la comunidad", "Question45"),
                            ("Infraestructura f�sica y medio ambiente", "Question46"),
                            ("Infraestructura f�sica y convivencia", "Question48"),
                            ("Indicadores Nacionales de Calidad Educativa", "Question49"),
    ])

    #TODO: Refactor this part
    sections_inv = OrderedDict([("Question1", "Informaci�n general del Establecimiento Educativo"),
                                ("Question9",
                                 "Integraci�n de la infraestructura f�sica al proceso de ense�anza y aprendizaje"),
                                ("Question37", "Infraestructura f�sica y calidad de vida y del empleo de los docentes"),
                                ("Question45", "Infraestructura f�sica y participaci�n y desarrollo de la comunidad"),
                                ("Question46", "Infraestructura f�sica y medio ambiente"),
                                ("Question48", "Infraestructura f�sica y convivencia"),
                                ("Question49", "Indicadores Nacionales de Calidad Educativa"),
    ])

    # Se usa un OrderedDict para conservar el �rden de las keys del diccionario.

    questions = ['Question1', 'Question4', 'Question5',
                 'Question6', 'Question7', 'Question8', 'Question9', 'Question10',
                 'Question11', 'Question12', 'Question13', 'Question14', 'Question15',
                 'Question16', 'Question17', 'Question19', 'Question20',
                 'Question21', 'Question22', 'Question23', 'Question24', 'Question25',
                 'Question26', 'Question27', 'Question28', 'Question29', 'Question30',
                 'Question31', 'Question32', 'Question100', 'Question33', 'Question34', 'Question35',
                 'Question36', 'Question37', 'Question38', 'Question39', 'Question40',
                 'Question41', 'Question42', 'Question43', 'Question44', 'Question45',
                 'Question46', 'Question47', 'Question48', 'Question49']
    #Mantener Siempre los elementos de esta lista en orden ascendente

    def __init__(self, *args, **kwargs):
        super(InstrumentoImpacto, self).__init__(*args, **kwargs)

    def rules_specification(self):
        self.rules = [
            (('Question9', 'Question10', 'Question22', 'Question37', 'Question41'), self._mostrar_preescolar_rule),
            (('Question11', 'Question12', 'Question19', 'Question23', 'Question26', 'Question29', 'Question30',
              'Question38', 'Question42'), self._mostrar_primaria_rule),
            (('Question13', 'Question14', 'Question20', 'Question24', 'Question27', 'Question31', 'Question39',
              'Question43'), self._mostrar_secundaria_rule),
            (('Question15', 'Question16', 'Question21', 'Question25', 'Question28', 'Question32', 'Question100', 'Question33',
              'Question34', 'Question35', 'Question36', 'Question40', 'Question44'), self._mostrar_media_rule)
        ]

        '''Viejas reglas
        self.rules_old = {
                'QPanel4P1': self._mostrar_preescolar_rule,
                'QPanel22P1': self._mostrar_preescolar_rule,
                'QPanel4P2': self._mostrar_primaria_rule,
                'QPanel22P2': self._mostrar_primaria_rule,
                'QPanel4P3': self._mostrar_secundaria_rule,
                'QPanel23P1': self._mostrar_secundaria_rule,
                'QPanel5': self._mostrar_media_rule,
                'QPanel23P2': self._mostrar_media_rule,
                'QPanel26P1': self._mostrar_preescolar_rule,
                'QPanel26P2': self._mostrar_primaria_rule,
                'QPanel26P3': self._mostrar_secundaria_rule,
                'QPanel26P4': self._mostrar_media_rule,
                'QPanel7P1': self._mostrar_educacion_p1,
                'QPanel7P2': self._mostrar_educacion_p3,
                'QPanel7P3': self._mostrar_educacion_p4,
                'QPanel8P1': self._mostrar_educacion_p6,
                'QPanel8P2': self._mostrar_educacion_p5,
                'QPanel8P3': self._mostrar_educacion_p2,
                }
        '''

    # Dependency rules definition

    # Pregunta 8. Grado rules
    Q8_ID, SQ8_ID = 8, 1

    def _mostrar_grado_rule(self, grado):
        """ generic rule to show panels depending on grado """
        qdata = self.get_question_data(InstrumentoImpacto.Q8_ID)
        sqdata = self.get_subq_data(qdata, InstrumentoImpacto.SQ8_ID)
        if sqdata is None: return False
        if grado in sqdata:
            return True
        return False

    def _mostrar_preescolar_rule(self):
        return self._mostrar_grado_rule(ct.PREESCOLAR)

    def _mostrar_primaria_rule(self):
        return self._mostrar_grado_rule(ct.BASICA_PRIMARIA)

    def _mostrar_secundaria_rule(self):
        return self._mostrar_grado_rule(ct.BASICA_SECUNDARIA)

    def _mostrar_media_rule(self):
        return self._mostrar_grado_rule(ct.MEDIA)

    # Pregunta 20. rules
    Q20_ID, SQ20_ID = 20, 1

    def _mostrar_educacion_rule(self, tipo):
        qdata = self.get_question_data(InstrumentoImpacto.Q20_ID)
        sqdata = self.get_subq_data(qdata, InstrumentoImpacto.SQ20_ID)
        if sqdata is None: return False
        if tipo in sqdata:
            return True
        return False

    def _mostrar_educacion_p1(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[0])

    def _mostrar_educacion_p2(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[1])

    def _mostrar_educacion_p3(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[2])

    def _mostrar_educacion_p4(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[3])

    def _mostrar_educacion_p5(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[4])

    def _mostrar_educacion_p6(self):
        return self._mostrar_educacion_rule(ct.EDUCACION_TIPO[5])
