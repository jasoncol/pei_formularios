::Requerimientos: python27 32bit, kivy 1.8, requests, tkinter

::Ruta Python
SET python="C:\Python27\python.exe"
SET project_path="../main.py"
SET icon_path="../img/icono.ico"
pyinstaller --name pei_formularios %project_path% --icon %icon_path%
%python% fix_spec.py
pyinstaller pei_formularios.spec
