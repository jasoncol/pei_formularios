# -*- coding: utf-8 -*-
""" Este módulo contiene las definiciones para el panel inicial de la app. """
from kivy.uix.anchorlayout import AnchorLayout
from kivy.properties import ObjectProperty
from instrument.instrumento_impacto import InstrumentoImpacto
from instrument.instrument2 import Instrument2
from instrument.base import INSTRUMENT_TYPE_1, INSTRUMENT_TYPE_2


class InitialPanel(AnchorLayout):

    """Widget del Panel inicial. """

    general_instrument_panel = ObjectProperty(None)
    """Referencia al panel general del instrumento."""

    instrument_panelmgr = ObjectProperty(None)
    """Referencia al panel de diligenciamiento de preguntas"""

    send_instrument_panel = ObjectProperty(None)
    """Referencia al panel de envío  de preguntas"""

    general_tab = ObjectProperty(None)
    instrument_mgr_tab = ObjectProperty(None)
    instrument_send_tab = ObjectProperty(None)
    load_instr_tab = ObjectProperty(None)

    tabpanel = ObjectProperty(None)
    """Referencia al Tabpanel de la UI."""

    def goto_load_instrument(self):
        """Muestra el panel de carga de instrumentos. Método que se llama cuando
        se presiona el botón de cargar instrumento.
        """
        self.tabpanel.switch_to(self.load_instr_tab)

    def new_instrument(self, type_):
        """Instancia un nuevo instrumento segun la clase del instrumento
        especificada (type_). Se llama cuando se presionan los botones
        para crear un nuevo instrumento.

        :param type_: Clase del instrumento (ej instrument.InstrumentoImpacto).
        :returns: None
        """

        if (type_ == 1):
            new_instrument = InstrumentoImpacto()
            name = "Nuevo Instrumento Impacto"
            t = INSTRUMENT_TYPE_1
        elif (type_ == 2):
            new_instrument = Instrument2()
            name = "Nuevo Instrumento Tipo II"
            t = INSTRUMENT_TYPE_2
        else:
            raise Exception

        new_instrument.name = name
        new_instrument.fname = name + ".pei"
        new_instrument.instrument_type = t
        new_instrument.date = "2015/Enero/1"  # TODO fecha actual
        new_instrument.submitter_name = "Nombre encuestador"
        new_instrument.construction_start_date = "2015/Enero/1"
        new_instrument.construction_finish_date = "2015/Enero/1"
        new_instrument.observations = '-'
        new_instrument.intervention_type = 7

        self.general_instrument_panel.load_instrument(new_instrument)
        self.instrument_panelmgr.load_instrument(new_instrument)
        self.general_tab.disabled = False
        self.instrument_mgr_tab.disabled = False
        self.instrument_send_tab.disabled = False
        self.send_instrument_panel.instrument = new_instrument

        self.tabpanel.switch_to(self.general_tab)
