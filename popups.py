# -*- coding: latin-1 -*-
""" M�dulo con mensajes del tipo popup para usar en la aplicaci�n
"""

from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout


class Popups:
    YES = 'Si'
    NO = 'No'

    @classmethod
    def info_popup(cls, title='Info', msg=' ', close_txt='Cerrar'):
        close_btn = Button(text=close_txt, size_hint=(1, 0.4))
        confirmation_msg = Label(text=msg)

        content = BoxLayout(orientation='vertical')
        content.add_widget(confirmation_msg)
        content.add_widget(close_btn)

        info_popup = Popup(title=title, content=content, size_hint=(0.5, 0.5))
        close_btn.bind(on_press=info_popup.dismiss)
        info_popup.open()

    @classmethod
    def yes_no_popup(cls, title=' ?', msg=' ', callback=None):
        yes_no = False
        yes_btn = Button(text=Popups.YES, size_hint=(0.5, 0.4))
        no_btn = Button(text=Popups.NO, size_hint=(0.5, 0.4))
        confirmation_msg = Label(text=msg)

        buttons = BoxLayout()
        buttons.add_widget(yes_btn)
        buttons.add_widget(no_btn)

        content = BoxLayout(orientation='vertical')
        content.add_widget(confirmation_msg)
        content.add_widget(buttons)

        yes_no_popup = Popup(title=title, content=content, size_hint=(0.5,0.5), auto_dismiss=False)

        yes_btn.bind(on_press=yes_no_popup.dismiss)
        no_btn.bind(on_press=yes_no_popup.dismiss)

        if callback is not None:
            yes_btn.bind(on_press=callback)
            no_btn.bind(on_press=callback)
 
        yes_no_popup.open()

        #todo: Hacer que el popup devuelva true o false segun el boton que se presione
        return yes_no
